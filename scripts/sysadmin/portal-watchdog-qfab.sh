#!/bin/sh
INSTANCE="dev"
SCRIPTDIR="/data/repo/mercurial/ASCC/scripts"

if [ `whoami` != "root" ]; then
  echo "Must be root (or sudo)!"; echo
  exit 1
fi

if [ -d $SCRIPTDIR ]; then
  cd $SCRIPTDIR
  su portaladmin -c "export USER=\"portaladmin\"; ./portal-admin -c QFAB $INSTANCE status | grep -i \"not running\" > /dev/null 2>&1"
  if [ $? -eq 0 ]; then
    logger -t PORTAL "Portal watchdog discovered portal not running, attempting to reboot now."
    ## Not running, attempt re-deployment
    #su portaladmin -c "export USER=\"portaladmin\"; ./portal-admin -c QFAB $INSTANCE restart"
    su portaladmin -c "export USER=\"portaladmin\"; ./portal-admin -c QFAB $INSTANCE restart > /dev/null 2>&1"

  ### DEBUG ###
  #else
  #   echo "DEBUG: Portal running, watchdog takes no action."
  fi
fi

