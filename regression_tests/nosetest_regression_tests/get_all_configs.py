
from guide.model.stemformatics import *


uid = 3


def test_configs():
    result = Stemformatics_Admin.get_all_configs()
    assert 'turn_on_google_analytics' in result
    assert 'site_name' in result

    ref_type = None
    ref_id = ''
    result = Stemformatics_Admin.edit_config(ref_type,ref_id)
    assert result == None


    ref_type =  88
    ref_id = ''
    result = Stemformatics_Admin.edit_config(ref_type,ref_id)
    assert result == None

    ref_type = 'nothing'
    ref_id = ''
    result = Stemformatics_Admin.edit_config(ref_type,ref_id)
    assert result == None



    ref_type = 'validator_url'
    ref_id = 'http://agile.stemformatics.org/validator/index.php?action123'
    result = Stemformatics_Admin.edit_config(ref_type,ref_id)
    assert result == True

    config = Stemformatics_Admin.get_all_configs()
    assert config['validator_url']  == ref_id



    ref_type = 'validator_url'
    ref_id = 'http://agile.stemformatics.org/validator/index.php?action'
    result = Stemformatics_Admin.edit_config(ref_type,ref_id)
    assert result == True

    config = Stemformatics_Admin.get_all_configs()
    assert config['validator_url']  == ref_id


