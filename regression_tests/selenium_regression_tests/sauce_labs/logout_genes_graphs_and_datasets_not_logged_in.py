def run_tests(self):
    driver = self.driver
    driver.get(self.base_url + "/auth/logout")
    driver.find_element_by_id("header-genesearch-button").click()
    driver.find_element_by_id("geneSearch").clear()
    driver.find_element_by_id("geneSearch").send_keys("stat1")
    driver.find_element_by_css_selector("#viewGenes > span").click()
    driver.find_element_by_css_selector("div.cells").click()
    driver.find_element_by_css_selector("span.clickable_div").click()
    driver.find_element_by_css_selector("div.inner > div.title").click()
    self.assertEqual("Stemformatics", driver.title)
    driver.find_element_by_id("header-datasetsearch-button").click()
    driver.find_element_by_xpath("//div[@id='main_body']/div[5]/div[2]/div[5]/a/span").click()
    driver.find_element_by_css_selector("div.inner > div.title").click()
    driver.find_element_by_id("geneSearch").clear()
    driver.find_element_by_id("geneSearch").send_keys("gapdh")
    driver.find_element_by_css_selector("#viewGenes > span").click()
    driver.find_element_by_link_text("Gapdh").click()
    self.assertEqual("Stemformatics", driver.title)

# ################################################################
# Need to add in the function above that has all the details here 
# It must be named run_tests eg.
# def run_tests(self):
#     driver = self.driver 
# ################################################################

import os
import sys
import httplib
import base64
import json
import new
import unittest
import sauceclient
from selenium import webdriver
from sauceclient import SauceClient

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

# Testing data stored in test_data.py
from defaults.test_data import test_data
test_data_object = test_data()
USERNAME = test_data_object.USERNAME
ACCESS_KEY = test_data_object.ACCESS_KEY
sauce = SauceClient(USERNAME, ACCESS_KEY)
browsers = test_data_object.browsers

def on_platforms(platforms):
    def decorator(base_class):
        module = sys.modules[base_class.__module__].__dict__
        for i, platform in enumerate(platforms):
            d = dict(base_class.__dict__)
            d['desired_capabilities'] = platform
            name = "%s_%s" % (base_class.__name__, i + 1)
            module[name] = new.classobj(name, (base_class,), d)
    return decorator

@on_platforms(browsers)
class NewClassNameHere(unittest.TestCase):
    # ################################################################
    # Need to add in the new class that has all the details here
    # ################################################################
    def test_to_run(self):
        run_tests(self)

    def setUp(self):
        self.desired_capabilities['name'] = self.id()

        self.base_url = "http://www1.stemformatics.org/"
        sauce_url = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub"
        self.driver = webdriver.Remote(
            desired_capabilities=self.desired_capabilities,
            command_executor=sauce_url % (USERNAME, ACCESS_KEY)
        )
        self.driver.implicitly_wait(30)

   
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        print("Link to your job: https://saucelabs.com/jobs/%s" % self.driver.session_id)
        try:
            if sys.exc_info() == (None, None, None):
                sauce.jobs.update_job(self.driver.session_id, passed=True)
            else:
                sauce.jobs.update_job(self.driver.session_id, passed=False)
        finally:
            self.driver.quit()




if __name__ == "__main__":
    unittest.main()
