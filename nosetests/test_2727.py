import unittest
import logging
log = logging.getLogger(__name__)

import string
import json
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *
from guide.model.graphs import *


def test_check_data_returned_for_many_buttons():
    format_type = 'front_end'
    dict_of_ds_ids = {}
    dict_of_ds_ids[7134] = {'dataset_status':'Limited'}
    from guide.model.stemformatics.stemformatics_dataset import Stemformatics_Dataset
    result = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
    print(result[7134]['showButtonOnDatasetSummaryPage'])
    assert result[7134]['showButtonOnDatasetSummaryPage'][0] == ('Glimma MDS', 'http://www.google.com')
    assert result[7134]['showButtonOnDatasetSummaryPage'][1] == ('Glimma Volcano' , 'http://www.google.co.in')
    assert result[7134]['showButtonOnDatasetSummaryPage'][2] == ('PCA' , 'http://www.google.co.au')

def test_check_data_returned_for_one_buttons():
    format_type = 'front_end'
    dict_of_ds_ids = {}
    dict_of_ds_ids[7132] = {'dataset_status':'Limited'}
    from guide.model.stemformatics.stemformatics_dataset import Stemformatics_Dataset
    result = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
    assert result[7132]['showButtonOnDatasetSummaryPage'][0] == ('PCA' , 'http://www.google.co.au')

def test_check_data_returned_for_no_button():
    format_type = 'front_end'
    dict_of_ds_ids = {}
    dict_of_ds_ids[5030] = {'dataset_status':'Limited'}
    from guide.model.stemformatics.stemformatics_dataset import Stemformatics_Dataset
    result = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
    assert result[5030]['showButtonOnDatasetSummaryPage'] == []
