from guide.model.stemformatics import *
from pylons import config
from guide.model.error import GeneralS4MError
from guide.model.graphs import *



uid = 3 #rowlandm
param_view_by=0 #yugene default
param_show_lower='Dataset' #yugene default


class tempData(object):
    pass


species_dict = Stemformatics_Gene.get_species(db)

temp = tempData() 

def test_mouse_geg():
    temp.ds_id = 4000
    temp.db_id = 46
    temp.ensembl_id = 'ENSMUSG00000030142'
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 1
    temp.expected_data_levels = 2
    temp.expected_handle ='Asselin-Labat_2010_20383121' 
    temp.expected_x_axis_label = 'MaSC enriched (CD29 hi CD24+)'
    temp.expected_title_id = ' for Gene Clec4e'
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = 'normal' 
    temp.expected_probe_list = [u'ILMN_1215209', u'ILMN_1216972']

    setup_graphs(temp)

def test_human_geg():
    temp.ds_id = 6076
    temp.db_id = 56
    temp.ensembl_id = 'ENSG00000111640'
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 102
    temp.expected_data_levels = 2
    temp.expected_handle ='FantomIPS_unpublished_PRIVATE' 
    temp.expected_x_axis_label = 'DSiPSC'
    temp.expected_title_id = ' for Gene GAPDH'
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = "Down's Syndrome" 
    temp.expected_probe_list = [u'p1@5end_chr12:6643676..6643693,+', u'p2@5end_chr12:6647043..6647106,+', u'p6@5end_chr12:6644397..6644439,+', u'p7@5end_chr12:6647475..6647494,+', u'p8@5end_chr12:6646104..6646127,+', u'p9@5end_chr12:6644443..6644469,+', u'p12@5end_chr12:6644523..6644576,+', u'p13@5end_chr12:6645636..6645677,+', u'p18@5end_chr12:6646472..6646496,+', u'p20@5end_chr12:6644473..6644488,+', u'p34@5end_chr12:6644390..6644395,+', u'p39@5end_chr12:6644510..6644511,+']
    setup_graphs(temp)


def setup_graphs(temp):
    this_graph_data = Graph_Data(db,temp.ds_id,temp.ref_type,temp.ref_id,temp.db_id,temp.list_of_samples_to_remove,species_dict)
    assert this_graph_data.handle == temp.expected_handle 
    assert this_graph_data.ref_type == temp.ref_type
    assert this_graph_data.ref_id == temp.ref_id
    assert this_graph_data.db_id == temp.db_id
    assert this_graph_data.chip_type == temp.expected_chip_type


    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels
    print this_graph.graph_data.probe_list
    assert this_graph.graph_data.probe_list == temp.expected_probe_list

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Box_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Bar_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels

    print this_view.graph.graph_data.probe_list
    assert this_view.graph.graph_data.probe_list == temp.expected_probe_list

    # disease state
    temp.sortBy = 'Disease State'
    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_disease_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_disease_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    # line graph - leave for now.

