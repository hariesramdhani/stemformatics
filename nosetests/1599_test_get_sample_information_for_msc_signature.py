from guide.model.stemformatics import *
from guide.model.graphs import *
import psycopg2
import psycopg2.extras

species_dict = Stemformatics_Gene.get_species(db)

class tempData(object):
    pass


temp = tempData() 

"""
Sql to setup the datasets
--delete from dataset_metadata where ds_name = 'RohartMSCAccess';
--insert into dataset_metadata (select id,'RohartMSCAccess','True' from datasets where show_yugene is True and db_id = 56);
--insert into dataset_metadata (select id,'RohartMSCAccess','False' from datasets where show_yugene is False);
--insert into dataset_metadata (select id,'RohartMSCAccess','False' from datasets where db_id = 46);
--select * from dataset_metadata where ds_name = 'RohartMSCAccess';
"""

def test_all_datasets():

    list_of_ds_ids = Stemformatics_Msc_Signature.get_all_dataset_msc_access()
    assert 6037 in list_of_ds_ids
    assert 6082 not in list_of_ds_ids
    assert 4000 not in list_of_ds_ids


def test_get_biosamples_metadata():

    result = Stemformatics_Dataset.getBiosamplesMetadata(db,ds_id=6037)
    temp_result = Stemformatics_Dataset.get_biosamples_metadata(db,ds_id=6037)
    assert len(result) == 600
    assert result == temp_result


def test_update_single_field():
    updated_field = 'RohartMSCAccess'
    new_value = 'False'
    uid = 3
    ds_id = 6037
    update_dict = {}
    update_dict[updated_field] = new_value
    result = Stemformatics_Dataset.update_dataset_single_field(db,uid,ds_id,update_dict)
    assert result == True


    conn_string = config['psycopg2_conn_string']
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "select * from dataset_metadata where ds_id = 6037 and ds_name = 'RohartMSCAccess';"
    cursor.execute(sql)
    # retrieve the records from the database
    temp_result = cursor.fetchall()
    value = temp_result[0]['ds_value']
    assert value == 'False'

    updated_field = 'RohartMSCAccess'
    new_value = 'True'
    uid = 3
    ds_id = 6037
    update_dict = {}
    update_dict[updated_field] = new_value
    result = Stemformatics_Dataset.update_dataset_single_field(db,uid,ds_id,update_dict)
    assert result == True

    conn_string = config['psycopg2_conn_string']
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "select * from dataset_metadata where ds_id = 6037 and ds_name = 'RohartMSCAccess';"
    cursor.execute(sql)
    # retrieve the records from the database
    temp_result = cursor.fetchall()
    value = temp_result[0]['ds_value']
    assert value == 'True'

    updated_field = 'RohartMSCAccess'
    new_value = 'False'
    uid = 44
    ds_id = 6037
    update_dict = {}
    update_dict[updated_field] = new_value
    result = Stemformatics_Dataset.update_dataset_single_field(db,uid,ds_id,update_dict)
    assert result == False

    updated_field = 'RohartMSCAccess'
    new_value = 'False'
    uid = 3
    ds_id = 60370
    update_dict = {}
    update_dict[updated_field] = new_value
    result = Stemformatics_Dataset.update_dataset_single_field(db,uid,ds_id,update_dict)
    assert result == False



def test_msc_lwr_upr():

    lwr_upr_values = Stemformatics_Msc_Signature.get_lwr_upr_msc_test()
    assert lwr_upr_values == {'lwr':0.4337,'upr':0.5169}

def get_sample_information(dict_of_values):
    ds_id = dict_of_values['ds_id']
    uid = dict_of_values['uid'] 
    chip_id = dict_of_values['chip_id']
    rep_id = dict_of_values['rep_id']
    sample_type = dict_of_values['sample_type']
    sample_info = Stemformatics_Msc_Signature.get_sample_information_for_msc_signature(db,ds_id,uid)
    assert chip_id in sample_info
    assert sample_info[chip_id]['Replicate Group ID'] == rep_id
    assert sample_info[chip_id]['Sample Type'] == sample_type



def test_get_sample_information():
    dict_of_values = {'uid':3,'ds_id':6037,'chip_id':'GSM250021','rep_id':'BM MSC, donor 3','sample_type':'BM MSC'}
    get_sample_information(dict_of_values)

    dict_of_values = {'uid':3,'ds_id':4000,'chip_id':'4936586022_E','rep_id':'luminal_GSM511134','sample_type':'luminal (CD29 lo CD24+)'}
    get_sample_information(dict_of_values)





def get_msc_values(dict_of_values):

    uid = dict_of_values['uid'] 
    ds_id = dict_of_values['ds_id']
    values_pred = dict_of_values['values']
    values = Stemformatics_Msc_Signature.get_msc_values(db,ds_id,uid)

    assert values == values_pred





def test_get_msc_values():
    #ds_id 6037

    values = "MSC_Type\tReplicate_Group_ID\tSample_Type\tchip_id\tclassification\tprediction\tlwr\tupr\tmissing_signature_genes\nMSC\tBM MSC, donor 1\tBM MSC\tGSM250019\t1\t0.600856827182859\t0.572170457367557\t0.633936513463107\t0\nNon-MSC\tBM MSC, donor 2\tBM MSC\tGSM250020\t2\t0.195395858322351\t0.134076689319985\t0.255711020002719\t0\nMSC\tBM MSC, donor 3\tBM MSC\tGSM250021\t1\t0.615459753851665\t0.585562143735879\t0.648917533579191\t0\nNon-MSC\tBM erythropoietic cells CD235A+, donor 1\tBM erythropoietic cells CD235A+\tGSM250022\t2\t0.0024744241163831\t-0.027907186787775\t0.0228126665844507\t0\nNon-MSC\tBM erythropoietic cells CD235A+, donor 2\tBM erythropoietic cells CD235A+\tGSM250023\t2\t-0.0272159124370074\t-0.0528487662462533\t-0.00824896692507485\t0\nNon-MSC\tBM erythropoietic cells CD235A+, donor 3\tBM erythropoietic cells CD235A+\tGSM250024\t2\t-0.173704632563746\t-0.209354211086638\t-0.144157668395836\t0\nNon-MSC\tBM granulopoietic cells CD11B+, donor 1\tBM granulopoietic cells CD11B+\tGSM250025\t2\t0.055291351514157\t0.0204344703102805\t0.0972177280408137\t0\nNon-MSC\tBM granulopoietic cells CD11B+, donor 2\tBM granulopoietic cells CD11B+\tGSM250026\t2\t-0.00964333322147842\t-0.0393256000966129\t0.0235368317369856\t0\nNon-MSC\tBM granulopoietic cells CD11B+, donor 3\tBM granulopoietic cells CD11B+\tGSM250027\t2\t0.00389214125181131\t-0.0411861481047381\t0.0507806545954915\t0\nNon-MSC\tBM hematopoietic cells CD45+, donor 1\tBM hematopoietic cells CD45+\tGSM250028\t2\t-0.0595867291261218\t-0.0922188551421025\t-0.0263467934586311\t0\nNon-MSC\tBM hematopoietic cells CD45+, donor 2\tBM hematopoietic cells CD45+\tGSM250029\t2\t-0.0279546395105703\t-0.0509223657769214\t0.000561992349002956\t0\nNon-MSC\tBM hematopoietic cells CD45+, donor 3\tBM hematopoietic cells CD45+\tGSM250030\t2\t-0.0592949996719657\t-0.0865139917496139\t-0.030119701719694\t0\nNon-MSC\tDeveloping cortex derived neural progenitor cells, replicate 1\tDeveloping cortex neural progenitor cells\tGSM335938\t2\t-0.557450060286944\t-0.609589220023941\t-0.51986098663744\t0\nNon-MSC\tVentral midbrain derived neural progenitor cells, replicate 1\tVentral midbrain neural progenitor cells\tGSM335940\t2\t-0.614153888294293\t-0.670694376145711\t-0.577010604901102\t0\nNon-MSC\tDeveloping cortex derived neural progenitor cells, replicate 2\tDeveloping cortex neural progenitor cells\tGSM335941\t2\t-0.492898463220494\t-0.548728717644806\t-0.454491156586065\t0\nNon-MSC\tVentral midbrain derived neural progenitor cells, replicate 2\tVentral midbrain neural progenitor cells\tGSM335942\t2\t-0.591883487299\t-0.649369987905623\t-0.550637949682265\t0\nUnsure\tOlfactory lamina propria derived stem cells, donor 1\tOlfactory lamina propria derived stem cells\tGSM606419\t2\t0.476954923300295\t0.451033459149881\t0.499345917667465\t0\nUnsure\tOlfactory lamina propria derived stem cells, donor 2\tOlfactory lamina propria derived stem cells\tGSM606420\t2\t0.49171315367664\t0.465955645294205\t0.515455073080384\t0\nUnsure\tOlfactory lamina propria derived stem cells, donor 3\tOlfactory lamina propria derived stem cells\tGSM606421\t1\t0.514025040508404\t0.49276077376909\t0.539465983316535\t0\nMSC\tBM MSC, donor 4\tBM MSC\tGSM606422\t1\t0.666903763826221\t0.63295206585103\t0.696020106755489\t0\nMSC\tBM MSC, donor 5\tBM MSC\tGSM606423\t1\t0.861550628505825\t0.832708285123198\t0.882399978708228\t0\nMSC\tBM MSC, donor 6\tBM MSC\tGSM606424\t1\t0.821216307729184\t0.795615924691369\t0.84278589254699\t0\nMSC\tBM MSC, donor 7\tBM MSC\tGSM606425\t1\t0.778108924867916\t0.736648787604726\t0.802826503619801\t0\nMSC\tBM MSC, donor 8\tBM MSC\tGSM606426\t1\t0.771576319821731\t0.744045563903394\t0.794724316563048\t0\nMSC\tBM MSC, donor 9\tBM MSC\tGSM606427\t1\t0.692742221764612\t0.66579376544358\t0.714273947083702\t0\n"


    dict_of_values = {'uid':3,'ds_id':6037,'values':values}
    get_msc_values(dict_of_values)

    dict_of_values = {'uid':0,'ds_id':6037,'values':values}
    get_msc_values(dict_of_values)

    # Note that this will need to be changed once we have all the files
    error = {'error': 'This dataset is not available for msc access.'}
    dict_of_values = {'uid':0,'ds_id':4000,'values':error}
    get_msc_values(dict_of_values)

    error = {'error': 'This dataset is not available for msc access.'}
    dict_of_values = {'uid':0,'ds_id':6082,'values':error}
    get_msc_values(dict_of_values)

    error = {'error': 'This dataset is not available for msc access.'}
    dict_of_values = {'uid':0,'ds_id':'abc','values':error}
    get_msc_values(dict_of_values)


def availability_access(dict_of_values):
    uid = dict_of_values['uid'] 
    ds_id = dict_of_values['ds_id']
    available = Stemformatics_Dataset.check_dataset_availability(db,uid,ds_id)
    assert available == dict_of_values['dataset_access']

    available = Stemformatics_Msc_Signature.get_dataset_msc_access(db,ds_id,uid)
    assert available == dict_of_values['msc_access']






def test_get_correct_ds_ids():
    dict_of_values = {'uid':3,'ds_id':6468,'dataset_access':True,'msc_access':True}
    availability_access(dict_of_values)

    dict_of_values = {'uid':0,'ds_id':6468,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)

    dict_of_values = {'uid':3,'ds_id':4000,'dataset_access':True,'msc_access':False}
    availability_access(dict_of_values)

    #yes dataset but no msc
    dict_of_values = {'uid':3,'ds_id':6082,'dataset_access':True,'msc_access':False}
    availability_access(dict_of_values)

    # no access for uid but access to msc
    dict_of_values = {'uid':0,'ds_id':5037,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)

    # no access for both
    dict_of_values = {'uid':0,'ds_id':6070,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)


    # check error of ds_id
    dict_of_values = {'uid':0,'ds_id':'abc','dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)
    dict_of_values = {'uid':0,'ds_id':None,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)

    # check ds_id is not available
    dict_of_values = {'uid':0,'ds_id':0,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)

    # fake uid
    dict_of_values = {'uid':None,'ds_id':4000,'dataset_access':True,'msc_access':False}
    availability_access(dict_of_values)
    dict_of_values = {'uid':'hello','ds_id':4000,'dataset_access':True,'msc_access':False}
    availability_access(dict_of_values)
    dict_of_values = {'uid':'None','ds_id':4000,'dataset_access':True,'msc_access':False}
    availability_access(dict_of_values)
    dict_of_values = {'uid':'hello','ds_id':6082,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)
    dict_of_values = {'uid':'None','ds_id':6082,'dataset_access':False,'msc_access':False}
    availability_access(dict_of_values)

