
from guide.model.stemformatics import *

from pylons import config



def test_get_first_kegg_gene_list():
    db_id = 56 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result != None
    assert len(result) == 1
    assert result[0]['db_id'] == db_id


    db_id = 46 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result != None
    assert len(result) == 1
    assert result[0]['db_id'] == db_id



    db_id = 16 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []
 
    db_id = '16'
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []


    db_id = None 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []

    db_id = 'abc'
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []

    db_id = [] 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []

    db_id = {} 
    result = Stemformatics_Gene_Set.get_first_kegg_gene_list(db_id)
    assert result == []
 
  

def test_get_species():
    db_id = 46
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert isinstance( ds_id,int )

    db_id = 56
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert isinstance( ds_id,int )


    db_id = '46'
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert isinstance( ds_id,int )

    db_id = '56'
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert isinstance( ds_id,int )

    db_id = ''
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert ds_id == None

    db_id = None 
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert ds_id == None

    db_id = 'abcddfd'
    ds_id = Stemformatics_Dataset.get_default_dataset(db_id)
    assert ds_id == None


    species_dict = Stemformatics_Gene.get_species(db)
    assert species_dict == {56: {'default_kegg_gene_list_id': 1785L, 'default_ds_id': 1000, 'name': u'Human', 'sci_name': u'Homo sapiens'}, 46: {'default_kegg_gene_list_id': 1560L, 'default_ds_id': 4000, 'name': u'Mouse', 'sci_name': u'Mus musculus'}} 

    

   
