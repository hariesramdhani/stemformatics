import unittest
import logging
log = logging.getLogger(__name__)

from bioblend import galaxy
from bioblend.galaxy.tools.inputs import inputs, dataset

from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *

def test_connect_to_galaxy():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    result = Stemformatics_Galaxy.connect_to_galaxy()
    assert result is not None

def test_run_GN_tool():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = galaxy.GalaxyInstance(url='http://115.146.95.203:8080/', key='f03bf2800c50683f79282bf180ac8d30')
    row = 'ILMN_1343291'
    uid = 629
    job_id= 18
    file_path = '/mnt/data/portal_data/pylons-data/prod/jobs/StemformaticsQueue/5138/job.gct'
    result = Stemformatics_Galaxy.run_GN_tool(galaxyInstance,job_id,file_path,uid,row)
    assert result is not None

def test_return_output_from_job():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = Stemformatics_Galaxy.connect_to_galaxy()
    job_list = {'5151':2}
    server_name = 'dev2-s4m'
    result = Stemformatics_Galaxy.return_job_status(galaxyInstance, job_list,server_name)
    assert result == {'5151': {'galaxy_job_id': u'96c64ae192f45ce4', 'state': u'ok', 'analysis_name': 'GN'}}

def test_update_job_status():
    from guide.model.stemformatics.stemformatics_galaxy import Stemformatics_Galaxy
    galaxyInstance = Stemformatics_Galaxy.connect_to_galaxy()
    status = {'5151': {'galaxy_job_id': u'96c64ae192f45ce4', 'state': u'ok', 'analysis_name': 'GN'}}
    result = Stemformatics_Galaxy.update_job_status(db,status,galaxyInstance)
    assert result is None

def test_get_job_view_result_for_HC():
    job_id = 5161
    job_details = Stemformatics_Job.get_job_details(db,job_id)
    result = Stemformatics_Job.get_job_view_result_for_HC(job_details)
    assert result == {'text_remove_sample_ids': 'No Samples Removed', 'text': {u'zero_var_columns': [], u'zero_var_rows': [], u'na_rows': [], u'row_ids': [u'SDHD ILMN_1698487', u'IDH3G ILMN_1802706', u'OGDH ILMN_1733869', u'OGDH ILMN_1783266', u'IDH3B ILMN_1774152', u'CS ILMN_1709775', u'PDHA2 ILMN_1665458', u'CS ILMN_1791182', u'PCK2 ILMN_1671791', u'PCK2 ILMN_1760649', u'IDH3A ILMN_1676609', u'IDH2 ILMN_1751753', u'ACLY ILMN_1749014', u'SUCLA2 ILMN_1660787', u'IDH3B ILMN_1721669', u'MDH2 ILMN_1694795', u'SUCLG1 ILMN_1779616', u'ACO1 ILMN_1750800', u'PDHA1 ILMN_1772369', u'PDHB ILMN_1739274', u'MDH1 ILMN_1656913', u'FH ILMN_1719392', u'DLAT ILMN_1736237', u'SDHA ILMN_1734640', u'ACO2 ILMN_1654861', u'IDH3A ILMN_1698533', u'SUCLG2 ILMN_1652379', u'SDHA ILMN_1744210', u'DLST ILMN_1773228', u'IDH1 ILMN_1696432', u'DLD ILMN_1664577', u'PC ILMN_1809417', u'PDHA1 ILMN_1671437', u'OGDHL ILMN_1714577', u'SDHA ILMN_1671972', u'IDH3G ILMN_1682762', u'SDHB ILMN_1667257', u'PC ILMN_1736689', u'SDHA ILMN_1651664', u'PCK1 ILMN_1731948']}, 'db_id': 56, 'cluster_type': u'pearson_both', 'colour_by': 'Row z-scores'}
