import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_gene_set.py')

human_db_id = 56
mouse_db_id = 46

gene_set_description_default = "THis is a test"

from matricks import *
import cPickle

import sys    

baseDir = "/home/s2776403/data/Stemformatics/GCTFiles/"    
# precompute cls files and make sure they are in the same order as other gct files
# pickle all datasets and store
# change code in api controller
# injecting gene names into gct files?    
    
def test_get_data_for_gct_download_success_and_failure_1000():
    '''
    db_id = 56
        
    # this is for local.ini
    gene_set_id = 115
    uid = 10
    
    dataset_id = 2000
    
    m_dataset = Matricks(baseDir+ 'dataset'+str(dataset_id)+'.pickled')
    
    probe_list = Stemformatics_Gene_Set.get_probes_from_gene_set_id(db,db_id,7,gene_set_id)
    
    f = open('/tmp/test.gct','w')
    
    m_dataset.extractRows(probe_list).gct(f)
    
    f.close()
    '''
    
def test_setup_gct_files():
    
    db.schema = 'public'
    ds = db.datasets
    
    result = ds.all()
    
    datasets = [str(dataset.id) for dataset in result ] 
        
    for dataset in datasets:
        f = open(baseDir+ 'dataset'+dataset+'.pickled','w')
        
        myGCT = Matricks(baseDir + 'dataset'+dataset+'.gct',skipcols=2)
        
        cPickle.dump(myGCT,f,-1)
        
        f.close()
    
