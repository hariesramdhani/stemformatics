
from guide.model.stemformatics import *

from pylons import config


def test_general_user_stats():
    
    
    start_date = '2015-04-01'
    end_date = '2015-06-30'
    limit = 20
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 2

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 2

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 2

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 2

    start_date = None
    end_date = '2015-06-30'
    limit = 20
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 0


    start_date = '2015-01-01'
    end_date = None
    limit = 20
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 0


    start_date = '2015-01-01'
    end_date = '2015-06-30'
    limit = None
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 0


    start_date = 'rowland-01-01'
    end_date = '2015-06-30'
    limit = 20
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    start_date = 'rowland-01-01'
    end_date = 'abcddf2015-06-30'
    limit = 20
    result = Stemformatics_Audit.get_user_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_dataset_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_gene_statistics(start_date,end_date,limit) 
    assert len(result) >= 0

    result = Stemformatics_Audit.get_controller_statistics(start_date,end_date,limit) 
    assert len(result) >= 0


