import cPickle

import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

mouse_db_id = 46
human_db_id = 56

import random
import inspect

import time

run_list = ['test_save_cpickle','test_load_cpickle']
# run_list = ['test_save_tsv','test_load_tsv']


# run_list = ['test_save_tsv']
run_list = ['test_load_tsv']

#run_list = ['test_load_cpickle']

dataDir = '/home/s2776403/Downloads/Ensembl/'
baseDir = '/tmp/'
    
    
def test_save_cpickle():
    
    name =  inspect.stack()[0][3]
    
    if name not in run_list:
        return
        
    
    f_gene_list = dataDir + '10000_gene_list_human.tsv'
    f_gene_sets = dataDir + 'db_id_56_all_genes.tsv'
    
    f_gl = open(f_gene_list,'r')
    
    gene_list = {}
    for line in f_gl:
        gene_list[line.replace('\n','')] = ''
        
    
    f_gs = open(f_gene_sets,'r')
    for line in f_gs:
        
        line = line.replace('\n','')
        row_list = line.split('\t')
        gene_id = row_list[0]
        
        if gene_id in gene_list:
            
            # print gene_id
            gene_list[gene_id] = row_list[1:]
            
            
        
    
    #print gene_list['ENSG00000143457']
    
    
    
    file_name =  baseDir + 'results.cpickle'
    f = open(file_name, 'wb')
    t0 = time.time()
    cPickle.dump(gene_list,f,-1)
    f.close()
    print 'picked file in', time.time() - t0, 'secs'

    
    
    
    


def test_load_cpickle():

    name =  inspect.stack()[0][3]
    
    if name not in run_list:
        return
        
    f_final = baseDir + 'results.cpickle'
    
    
    t0 = time.time()
    f = open(f_final, 'rb')
    gene_list = cPickle.load(f)
    print 'loaded pickled file in', time.time() - t0, 'secs'
    
    #print gene_list['ENSG00000143457']
    
   #assert True == False
        
    
    

    
    
def test_save_tsv():

    name =  inspect.stack()[0][3]
    
    if name not in run_list:
        return
        
        
    f_gene_list = dataDir + '10000_gene_list_human.tsv'
    f_gene_sets = dataDir + 'db_id_56_all_genes.tsv'
    
    f_gl = open(f_gene_list,'r')
    
    gene_list = {}
    for line in f_gl:
        gene_list[line.replace('\n','')] = ''
        
    
    f_gs = open(f_gene_sets,'r')
    for line in f_gs:
        line = line.replace('\n','')
        row_list = line.split('\t')
        gene_id = row_list[0]
        
        
        
        if gene_id in gene_list:
            
            # print gene_id
            gene_list[gene_id] = row_list[1:]
        
    
    #print gene_list['ENSG00000143457']
    
    
    file_name =  baseDir + 'results.tsv'
    f = open(file_name, 'w')
    for gene_id in gene_list:
        
        gene_sets = gene_list[gene_id]
        
        raw_line = gene_id + "\t" + "\t".join(gene_sets)+"\n"
        
        f.write(raw_line)
    
    

    
    
    
def test_load_tsv():    

    name =  inspect.stack()[0][3]
    
    if name not in run_list:
        return
        
        
    f_final = baseDir + 'results.tsv'
    f = open(f_final,'r')
    
    gene_list = {}
    for line in f:
        line = line.replace('\n','')
        row_list = line.split('\t')
        gene_id = row_list[0]
            
        # print gene_id
        
        gene_list[gene_id] = row_list[1:]
        
    
    
    #print gene_list['ENSG00000143457']
    
    #assert True == False
        
    
    
        
    
    

    
        
        
        
    
        
        
    
    
    
    
    
    
    
    
