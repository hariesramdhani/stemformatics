
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *
from guide.model.graphs import *



def test_deleting_data_from_redis():
    result = Stemformatics_Expression.delete_data_from_redis(["probe_graph_data","gene_mapping_data","gene_set_mapping_data","dataset_metadata"])
    print result
    assert result is not None

