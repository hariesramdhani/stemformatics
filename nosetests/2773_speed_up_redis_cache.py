from guide.model.stemformatics import *
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config
import psycopg2,psycopg2.extras,cPickle
import redis

species_dict = Stemformatics_Gene.get_species(db)

# alter table stemformatics.audit_log add column extra_ref_type text;
# alter table stemformatics.audit_log add column extra_ref_id text;

# https://dev.stemformatics.org/expressions/result?graphType=box&datasetID=6071&gene=ENSMUSG00000026104&db_id=46
# https://dev.stemformatics.org/expressions/result?graphType=box&datasetID=6071&gene=ENSMUSG00000030142&db_id=46
# https://dev.stemformatics.org/expressions/graph_data?db_id=46&ds_id=6128&ref_id=MI0000154&ref_type=miRNA&graph_type=box
# clear;redis-cli -s /data/redis/redis.sock GET "probe_graph_data|6071|ENSMUST00000087717|probeID|46"
# clear;redis-cli -s /data/redis/redis.sock GET "probe_graph_data|6071|ENSMUST00000070968|probeID|46"
# clear;redis-cli -s /data/redis/redis.sock GET "probe_graph_data|6071|ENSMUST00000032239|probeID|46"
# clear;redis-cli -s /data/redis/redis.sock GET "miRNA_mapping_data|6128|MI0000154|miRNA|46"

# -------------------------------------- Test pre_gene_search ---------------------------------------------------
all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()


def set_front_end_clear_redis(ds_id,db_id,data_type_id):

    date_to_calculate_from = '2017-09-20'
    delimiter = config['redis_delimiter']
    r_server = redis.Redis(unix_socket_path=config['redis_server'])

    result = r_server.get("dataset_mapping_data")
    mapping_id_dict = Stemformatics_Expression.unpickle_expression_data(result)
    mapping_id = mapping_id_dict[int(ds_id)]


    label = "dataset_metadata"+delimiter+str(ds_id)
    result = r_server.delete(label)



    conn_string = config['psycopg2_conn_string']
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    data = {"date": date_to_calculate_from,"ds_id":str(ds_id),"extra_ref_type":"gene_id"}
    cursor.execute("select extra_ref_id from stemformatics.audit_log where controller = 'expressions' and date_created >= %(date)s and ref_id = %(ds_id)s and extra_ref_type= %(extra_ref_type)s group by extra_ref_id;",data)

    # retrieve the records from the database
    result = cursor.fetchall()
    
    if result and isinstance(result[0],list):
        gene_id_list = []
        for row in result:
            gene_id = row[0]
            gene_id_list.append(gene_id)

        
        # if it's a normal dataset, it could have probes, so need to go down one level using feature_mappings.
        # If it's a miRNA dataset (data_type_id = 8) I don't ahve to do that as it stores it at the miRNA (gene equivalent) level.

        if data_type_id == 8:
            for gene_id in gene_id_list:
                label = "miRNA_mapping_data" + delimiter + str(ds_id) + delimiter + gene_id + delimiter + "miRNA" + delimiter + str(db_id)
                miRNA_result = r_server.delete(label)

        else:
            data = {"gene_id_list": gene_id_list,"mapping_id":mapping_id,"db_id":db_id}
            cursor.execute("select to_id from stemformatics.feature_mappings where mapping_id = %(mapping_id)s and db_id = %(db_id)s and from_type = 'Gene' and from_id = ANY (%(gene_id_list)s);",data)
            new_result = cursor.fetchall()
       
            if new_result:
                test_list = []
                for probe in new_result:

                    probe_graph_data_label = str("probe_graph_data"+delimiter+str(ds_id)+delimiter+str(probe[0])+delimiter+'probeID'+delimiter+str(db_id))
                    probe_result = r_server.delete(probe_graph_data_label)
                    test_list.append(probe_graph_data_label)                
 
    cursor.close()
    conn.close()





def test_initial():

    ds_id = 6071
    db_id = 46
    data_type_id = 0
    set_front_end_clear_redis(ds_id,db_id,data_type_id)

    ds_id = 6128
    db_id = 46
    data_type_id = 8
    set_front_end_clear_redis(ds_id,db_id,data_type_id)
