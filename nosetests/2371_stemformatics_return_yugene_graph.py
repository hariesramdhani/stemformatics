import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

species_dict = Stemformatics_Gene.get_species(db)

""" Testing the gene controller"""

"""
redis-cli -s /data/redis/redis.sock KEYS "sample_data|*" | xargs redis-cli -s /data/redis/redis.sock DEL
redis-cli -s /data/redis/redis.sock KEYS "full_data|*" | xargs redis-cli -s /data/redis/redis.sock DEL


"""


# -------------------------------------- Test pre_gene_search ---------------------------------------------------
all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
def test_all_non_logged_in_users():
    uid  = 0
    ensembl_id = 'ENSG00000229094'
    db_id = 56 
    role = 'normal'


    sample_values = Stemformatics_Expression.get_yugene_sample_data_graph_values(uid,ensembl_id,db_id)
    if sample_values is not None and sample_values != '[]':
        return True

    datasets_dict = Stemformatics_Dataset.get_all_x_platform_datasets_for_user(uid,db_id,role)
    assert 6616 not in datasets_dict
    assert 6631 not in datasets_dict
    assert 6614 in datasets_dict

    # graph_values has two keys, full_data and sample_data returned
    graph_values = Stemformatics_Expression.return_x_platform_matricks_data(db_id,datasets_dict,ensembl_id,all_sample_metadata)


    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)

    # get the full data, filter it, then get the metadata list and calculate the breakdown
    full_data = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)
    assert full_data is not None

def test_filtering():
    uid  = 3
    ensembl_id = 'ENSG00000229094'
    db_id = 56 
    role = 'admin'
    #initialisae the data into redis
    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)


    # get the full data, filter it, then get the metadata list and calculate the breakdown
    full_data = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)
    assert full_data is not None


    filters = {'filter_value_start':0.18,'filter_value_end':0.38} # start must have an end
    max_length = None
    max_length_action = 'truncate'


    filtered_result= Stemformatics_Expression.filter_yugene_graph(filters,db_id,full_data,max_length,max_length_action)
    metadata_list =  ['Tissue']
    final_data = Stemformatics_Expression.return_breakdown_of_filtered_results(metadata_list,all_sample_metadata,filtered_result)
 

    assert final_data == {'Tissue': {u'bone marrow': 3, u'white matter': 4, u'grey matter': 3}}

    format_type = 'tsv'
    data = Stemformatics_Expression.convert_yugene_breakdown_data_to_tsv_csv(final_data,format_type)

    assert data == u'type\tname\tnumber\nTissue\tbone marrow\t3\nTissue\twhite matter\t4\nTissue\tgrey matter\t3\n'



    filters = {'ds_ids':[5003],'value_start':0.99,'value_end':1.0}
    max_length = None
    max_length_action = 'truncate'

    filtered_result= Stemformatics_Expression.filter_yugene_graph(filters,db_id,full_data,max_length,max_length_action)
    metadata_list =  Stemformatics_Expression.get_all_metadata_list_for_x_platform()
    final_data = Stemformatics_Expression.return_breakdown_of_filtered_results(metadata_list,all_sample_metadata,filtered_result)
  
    assert final_data == True

    db_id = 46 
    ensembl_id = 'ENSMUSG00000030142'
    #initialisae the data into redis
    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)
    full_data = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)
    filters = {'value_start':0.99,'value_end':1.0}
    max_length = 1000
    max_length_action = 'truncate'
    filtered_result= Stemformatics_Dataset.filter_yugene_graph(filters,db_id,full_data,max_length,max_length_action)
    metadata_list =  Stemformatics_Expression.get_all_metadata_list_for_x_platform()
    breakdown = Stemformatics_Dataset.return_breakdown_of_filtered_results(metadata_list,all_sample_metadata,filtered_result)
    assert breakdown == {'Organism Part': {u'Bone marrow': 3, u'Peritoneum': 3, u'NULL': 1}, 'ds_id': {6344: 3, 6498: 3, 6311: 1}, 'Tissue': {u'NULL': 7}, 'Disease State': {u'Listeria monocytogenes (Lis)': 3, u'Normal': 4}, 'Sample Type': {u'Stat1-alpha KO + 6h IFNg + Listeria': 1, u'Sample Type': 1, u'Stat1KO +6hr IFNg + Listeria': 1, u'WT plus TDM': 3, u'WT BMM + 6hr IFNg + Listeria': 1}, 'Cell Type': {u'Peritoneal macrophage': 3, u'BMM (bone marrow derived macrophages)': 3, u'NULL': 1}}








def test_get_genes_search_ensembl():
    return
    uid  = 3
    ensembl_id = 'ENSG00000115415abc'
    db_id = 56 
    
    result = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)

    ensembl_id = 'ENSG00000115415'
    role = 'admin'
    param_view_by = None
    param_show_lower = None
    yugene_granularity_for_gene_search = 'auto'
    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)
    #assert len(result.graph_values) == 957761

 
    db_id = 46 
    ensembl_id = 'ENSMUSG00000057666'
    uid  = 0
    role = None
    param_view_by = None
    param_show_lower = None
    yugene_granularity_for_gene_search = 'auto'
    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)
    #assert len(result.graph_values) == 957761


