import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

species_dict = Stemformatics_Gene.get_species(db)

""" Testing the gene controller"""

# -------------------------------------- Test pre_gene_search ---------------------------------------------------

def test_pre_gene_search_symbols_and():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' $#$#    CDK11  ,*^   STAT, pinch-3')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&STAT&pinch-3'

def test_pre_gene_search_symbols_or():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' $#$#    CDK11,*^STAT ,pinch-3')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|STAT&pinch-3'


def test_pre_gene_search_symbols_crazy():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11&^^&^&^*23423STAT4234')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&23423STAT4234'

def test_pre_gene_search_symbols_crazier():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11^^|^^*23423(^%$#$$STAT4234^^^$$$###|ENSG00000166523')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|23423|STAT4234|ENSG00000166523'


def test_pre_gene_search_sql_injection():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch(' CDK11;drop probe_expressions;')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|drop&probe_expressions'



def test_pre_gene_search_basic():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11'

""" Check spaces turn into & for and"""
def test_pre_gene_search_spaces():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11 STAT1 STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&STAT1&STAT5'

""" Check commas turn into | for or"""
def test_pre_gene_search_comma():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11,STAT1,STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|STAT1|STAT5'


def test_pre_gene_search_pipe():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11|STAT1|STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11|STAT1|STAT5'

def test_pre_gene_search_ampasand():
    geneSearchFinal = Stemformatics_Gene._preGeneSearch('CDK11&STAT1&STAT5')
    print geneSearchFinal
    assert geneSearchFinal == 'CDK11&STAT1&STAT5'

# -------------------------------------- Test get_genes ---------------------------------------------------

def test_get_genes_search_ensembl():
    returnData = Stemformatics_Gene.get_genes(db,useSqlSoup,None,None,False,50)
    print returnData
    assert returnData == None 

def test_get_genes_search_ensembl():
    returnData = Stemformatics_Gene.get_genes(db,species_dict,'ENSG00000115415',None,False,50)
    print returnData
    assert returnData == {u'ENSG00000115415': {'samples_detected': '', 'refseq_dna_id': u' NM_007315 NM_139266', 'Location': {'direction': -1, 'end': 191885686, 'orientation': -1, 'start': 191829084, 'chr': u'2', 'strand': -1, 'chromosome': u'2'}, 'species': u'Homo sapiens', 'diseases': 'diseases go here (not included in genome_annotations)', 'EnsemblID': u'ENSG00000115415', 'id': u'ENSG00000115415', 'source': u'HGNC Symbol', 'Pathways': '', 'samples': '', 'official_symbol': u'STAT1', 'aliases': u'CANDF7 ISGF-3 STAT91', 'location': {'direction': -1, 'end': 191885686, 'orientation': -1, 'start': 191829084, 'chr': u'2', 'strand': -1, 'chromosome': u'2'}, 'status': u'KNOWN', 'db_id': 56, 'description': u'signal transducer and activator of transcription 1, 91kDa <br />', 'symbol': u'STAT1', 'biotype': u'protein_coding', 'EntrezID': u'6772', 'associated_db': u'ensembl', 'name': u'STAT1', 'alt_splicing': '', 'Synonyms': u'CANDF7 ISGF-3 STAT91', 'cage_data': ''}}



def test_get_genes_search_stat1():
    returnData = Stemformatics_Gene.get_genes(db,species_dict,'stat1',None,False,50)
    print returnData
    assert returnData == {u'ENSG00000115415': {'samples_detected': '', 'refseq_dna_id': u' NM_007315 NM_139266', 'Location': {'direction': -1, 'end': 191885686, 'orientation': -1, 'start': 191829084, 'chr': u'2', 'strand': -1, 'chromosome': u'2'}, 'species': u'Homo sapiens', 'diseases': 'diseases go here (not included in genome_annotations)', 'EnsemblID': u'ENSG00000115415', 'id': u'ENSG00000115415', 'source': u'HGNC Symbol', 'Pathways': '', 'samples': '', 'official_symbol': u'STAT1', 'aliases': u'CANDF7 ISGF-3 STAT91', 'location': {'direction': -1, 'end': 191885686, 'orientation': -1, 'start': 191829084, 'chr': u'2', 'strand': -1, 'chromosome': u'2'}, 'status': u'KNOWN', 'db_id': 56, 'description': u'signal transducer and activator of transcription 1, 91kDa <br />', 'symbol': u'STAT1', 'biotype': u'protein_coding', 'EntrezID': u'6772', 'associated_db': u'ensembl', 'name': u'STAT1', 'alt_splicing': '', 'Synonyms': u'CANDF7 ISGF-3 STAT91', 'cage_data': ''}, u'ENSG00000170581': {'samples_detected': '', 'refseq_dna_id': u' NM_005419 NM_198332', 'Location': {'direction': -1, 'end': 56753939, 'orientation': -1, 'start': 56735381, 'chr': u'12', 'strand': -1, 'chromosome': u'12'}, 'species': u'Homo sapiens', 'diseases': 'diseases go here (not included in genome_annotations)', 'EnsemblID': u'ENSG00000170581', 'id': u'ENSG00000170581', 'source': u'HGNC Symbol', 'Pathways': '', 'samples': '', 'official_symbol': u'STAT2', 'aliases': u'ISGF-3 P113 STAT113', 'location': {'direction': -1, 'end': 56753939, 'orientation': -1, 'start': 56735381, 'chr': u'12', 'strand': -1, 'chromosome': u'12'}, 'status': u'KNOWN', 'db_id': 56, 'description': u'signal transducer and activator of transcription 2, 113kDa <br />', 'symbol': u'STAT2', 'biotype': u'protein_coding', 'EntrezID': u'6773', 'associated_db': u'ensembl', 'name': u'STAT2', 'alt_splicing': '', 'Synonyms': u'ISGF-3 P113 STAT113', 'cage_data': ''}, u'ENSMUSG00000026104': {'samples_detected': '', 'refseq_dna_id': u' NM_001205313 NM_001205314 NM_009283', 'Location': {'direction': 1, 'end': 52218704, 'orientation': 1, 'start': 52176282, 'chr': u'1', 'strand': 1, 'chromosome': u'1'}, 'species': u'Mus musculus', 'diseases': 'diseases go here (not included in genome_annotations)', 'EnsemblID': u'ENSMUSG00000026104', 'id': u'ENSMUSG00000026104', 'source': u'MGI Symbol', 'Pathways': '', 'samples': '', 'official_symbol': u'Stat1', 'aliases': u'2010005J02Rik AA408197', 'location': {'direction': 1, 'end': 52218704, 'orientation': 1, 'start': 52176282, 'chr': u'1', 'strand': 1, 'chromosome': u'1'}, 'status': u'KNOWN', 'db_id': 46, 'description': u'signal transducer and activator of transcription 1 <br />', 'symbol': u'Stat1', 'biotype': u'protein_coding', 'EntrezID': u'20846', 'associated_db': u'ensembl', 'name': u'Stat1', 'alt_splicing': '', 'Synonyms': u'2010005J02Rik AA408197', 'cage_data': ''}} 



def test_get_genes_search_cdk11a_cdk11b():
    returnData = Stemformatics_Gene.get_genes(db,species_dict,'cdk11a',None,False,50)
    print returnData
    #assert returnData == {u'ENSG00000008128': {'samples_detected': '', 'refseq_dna_id': u' NM_024011 NM_033486 NM_033488 NM_033489 NM_033492 NM_033493 NM_033529  NM_024011 NM_033486 NM_033488 NM_033489 NM_033492 NM_033493 NM_033529', 'Location': {'direction': -1, 'end': 1655777, 'orientation': -1, 'start': 1634169, 'chr': u'1', 'strand': -1, 'chromosome': u'1'}, 'species': u'Homo sapiens', 'diseases': 'diseases go here (not included in genome_annotations)', 'EnsemblID': u'ENSG00000008128', 'id': u'ENSG00000008128', 'source': u'HGNC Symbol', 'Pathways': '', 'samples': '', 'official_symbol': u'CDK11A', 'aliases': u'CDC2L2 CDC2L3 CDK11-p110 CDK11-p46 CDK11-p58 PITSLRE p58GTA', 'location': {'direction': -1, 'end': 1655777, 'orientation': -1, 'start': 1634169, 'chr': u'1', 'strand': -1, 'chromosome': u'1'}, 'status': u'KNOWN', 'db_id': 56, 'description': u'cyclin-dependent kinase 11A <br />', 'symbol': u'CDK11A', 'biotype': u'protein_coding', 'EntrezID': u'728642', 'associated_db': u'ensembl', 'name': u'CDK11A', 'alt_splicing': '', 'Synonyms': u'CDC2L2 CDC2L3 CDK11-p110 CDK11-p46 CDK11-p58 PITSLRE p58GTA', 'cage_data': ''}}



def test_get_genes_search_stat_length():
    returnData = Stemformatics_Gene.get_genes(db,species_dict,'stat',None,False,50)
    print len(returnData)
    #assert len(returnData) == 33 



def test_get_genes_search_stat_length_limit_50():
    returnData = Stemformatics_Gene.get_genes(db,species_dict,'protein',None,False,50)
    print len(returnData)
    #assert len(returnData) == 50 

