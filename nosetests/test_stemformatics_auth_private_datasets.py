from guide.model.stemformatics import *

import logging
log = logging.getLogger(__name__)

import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

from guide.lib.state import *

useSqlSoup = True


""" Testing the auth model"""

# checks if an admin gets only the ones which they have specific access too
def test_private_datasets_admin():
    uid = 1
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_datasets = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)
    print("Testing admin user with annotate and view status and group and user permissions")
    #print(user_datasets)
    assert user_datasets[6117]['permissions'] == 'User'
    assert user_datasets[6117]['status'] == 'view'
    assert user_datasets[6089]['permissions'] == 'Group'
    assert user_datasets[6089]['status'] == 'view'
    assert user_datasets[6134]['permissions'] == 'Group'
    assert user_datasets[6134]['status'] == 'view'
    assert user_datasets[6135]['permissions'] == 'Group'
    assert user_datasets[6135]['status'] == 'view'
    assert user_datasets[6136]['permissions'] == 'Group'
    assert user_datasets[6136]['status'] == 'view'
    assert user_datasets[6101]['permissions'] == 'User'
    assert user_datasets[6101]['status'] == 'view'

    #assert len(user_datasets) == 6


    print("Testing an admin with no specific datasets")
    uid = 486
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_datasets = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)

    assert user_datasets == {}

# checks a normal user with access to some private datasets
def test_private_datasets_normal():
    uid = 297
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_datasets = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)
    print("Testing normal user with user permissions and view status")
    #print(user_datasets)
    assert user_datasets[6075]['permissions'] == 'User'
    assert user_datasets[6075]['status'] == 'view'

    assert user_datasets[5020]['permissions'] == 'User'
    assert user_datasets[5020]['status'] == 'view'

    assert user_datasets[6070]['permissions'] == 'User'
    assert user_datasets[6070]['status'] == 'view'

    assert user_datasets[6071]['permissions'] == 'User'
    assert user_datasets[6071]['status'] == 'view'

    #assert len(user_datasets) == 4



# Checks the function provides the right return for when the user has annotator
# role
def test_private_datasets_annotate():
    uid = 196
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_datasets = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)

    assert user_datasets[6075]['permissions'] == 'User'
    assert user_datasets[6075]['status'] == 'view'
    assert user_datasets[6075]['private'] == True

    assert user_datasets[6079]['permissions'] == 'User'
    assert user_datasets[6079]['status'] == 'view'
    assert user_datasets[6079]['private'] == True

    assert user_datasets[6237]['permissions'] == 'User'
    assert user_datasets[6237]['status'] == 'annotator'
    assert user_datasets[6237]['private'] == False

    assert user_datasets[6238]['permissions'] == 'User'
    assert user_datasets[6238]['status'] == 'annotator'
    assert user_datasets[6238]['private'] == False

    #assert len(user_datasets) == 2


# Checks for when the user is a guest
def test_private_datasets_guest():
    uid = 44
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_datasets = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)
    # print(user_datasets)
    assert user_datasets == {}


# Checks the sql statement returned the correct users
def test_get_private_datasets_from_db():
    uid = 3
    test_g = [6131, 'view', 'Group']
    test_u = [6098, 'view', 'User']
    user_override_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    assert len(user_override_list) == 30
    assert test_u in user_override_list
    assert test_g in user_override_list

# Checks creating the list
def test_user_private_dataset_access_dict():
    uid = 3
    test_6131 = {'status': u'view', u'Title': u'Project Grandiose: An epigenomic roadmap to induced pluripotency - MethylCpG', u'Organism': u'Mus musculus', 'permissions': u'Group', 'private': False}
    test_6098 = {'status': u'view', u'Title': u'Perivascular human endometrial mesenchymal stem cells express pathways relevant to self-renewal, lineage specification, and functional phenotype.', u'Organism': u'Homo sapiens', 'permissions': u'User', 'private': False}
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    user_override_dict = Stemformatics_Auth.get_dict_users_private_datasets_metadata(ds_list)
    assert user_override_dict[6131] == test_6131
    assert user_override_dict[6098] == test_6098
    assert len(user_override_dict.items()) == 21

# Checks users which don't exist are blocked
def test_user_no_access():
    uid = None
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid) 
    assert ds_list == None

    uid = 'ehlo'
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    assert ds_list == None

    uid = -97879
    ds_list = Stemformatics_Auth.get_list_users_private_datasets(uid)
    assert ds_list == None

