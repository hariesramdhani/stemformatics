from guide.model.stemformatics import *


rowland_home_ucsc_test = {'RNASeq Expression from Lifescope1': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/RNAseqCufflinks.20121109.visualisation/RNASeqCufflinks.txt', 'gid': 1, 'link_name': u'RNASeq Expression from Lifescope', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'Histone ChipSeq K27me31': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/chip_seq/K27me3_BigWigs/K27me3.txt', 'gid': 1, 'link_name': u'Histone ChipSeq K27me3', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'Histone ChipSeq K4me31': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/chip_seq/K4me3_BigWigs/K4me3.txt', 'gid': 1, 'link_name': u'Histone ChipSeq K4me3', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'RNA normalised coverage1': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/RNA_normalised_coverage/RNA_normalised_coverage_main.txt', 'gid': 1, 'link_name': u'RNA normalised coverage', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'RNASeq1': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/RNASeq_merged/RNASeq_merged.txt', 'gid': 1, 'link_name': u'RNASeq', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'Histone ChipSeq K36me31': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/chip_seq/K36me3_BigWigs/K36me3.txt', 'gid': 1, 'link_name': u'Histone ChipSeq K36me3', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'RNA normalised coverage (small libraries)1': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/RNA_normalised_coverage/RNA_normalised_coverage_small.txt', 'gid': 1, 'link_name': u'RNA normalised coverage (small libraries)', 'db_id': 46, 'group_name': u'Project Grandiose'}, 'CpG Methylome1': {'url': u'http://pipeline2.stemformatics.org/ucsc_tracks/ProjectGrandiose/cpg_methylome/cpg_methylome_new.txt', 'gid': 1, 'link_name': u'CpG Methylome', 'db_id': 46, 'group_name': u'Project Grandiose'}}


rowland_home_single_public_group_test = {u'new0': {'url': u'value', 'gid': 0, 'link_name': u'new', 'db_id': 46, 'group_name': u'Public'}}

ucsc_test = rowland_home_ucsc_test
new_test = rowland_home_single_public_group_test

def test_ucsc_links_before_public_group():
    uid = 3
    db_id = 46
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)


    # created a new UCSC links for Group 0
    # insert into stemformatics.group_configs values(0,'UCSC Links','new','value',46);
    del ucsc_links['new0']
    assert ucsc_links == ucsc_test


    uid = 0
    db_id = 46
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == new_test

    uid = 0
    db_id = 56
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = 3
    db_id = 56
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}



def test_errors():
    uid = 'abc'
    db_id = 46
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = None
    db_id = 46
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = 34242342342438436312894612364981236412341412341234143
    db_id = 46
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}



    uid = None
    db_id = None
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = 0
    db_id = None
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = None
    db_id = 56
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = 'abc'
    db_id = 56
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}

    uid = 34242342342438436312894612364981236412341412341234143
    db_id = 56
    ucsc_links = Stemformatics_Auth.get_ucsc_links_for_uid(db,uid,db_id)
    assert ucsc_links == {}



