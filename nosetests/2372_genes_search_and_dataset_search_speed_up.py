from guide.model.stemformatics import *

from pylons import config




def test_datasets_search():

    uid = 3
    searchQuery = 'mincle'
    filter_dict = {'show_limited':False,'rohart_msc_test':False}
    temp_data = Stemformatics_Dataset.search_and_choose_datasets(uid,searchQuery,filter_dict)
    assert temp_data == {'filter': 'mincle', 'datasets': {6498: {'cells_samples_assayed': u'Wild type and Mincle knockout Peritoneal macrophages', 'organism': u'Mus musculus', 'name': u'Tanaka_2014_25236782', 'title': u'Macrophage-inducible C-type lectin underlies obesity-induced adipose tissue fibrosis.'}, 6731: {'cells_samples_assayed': u'CD11b CD45int microglia, Clec4e knockout mouse', 'organism': u'Mus musculus', 'name': u'Manzanero_2015_S4M-6731_PRIVATE', 'title': u'The role of Clec4e (Mincle) in microglia response to transient ischemic injury'}, 6124: {'cells_samples_assayed': u'BMDM, macrophages', 'organism': u'Mus musculus', 'name': u'Hitchens_2012_unpublished_PRIVATE', 'title': u'Mincle TDM Timecourse'}}, 'order': {}}


    species_dict = Stemformatics_Gene.get_species(db)

    gene_search = 'stat'
    db_id = 46
    max_number = 20
    temp_data = Stemformatics_Gene.search_and_choose_genes(db,species_dict,gene_search,db_id,max_number)
    assert temp_data =={'filter': 'stat', 'genes': [{'ensembl_id': u'ENSMUSG00000026104', 'db_id': 46, 'description': u'signal transducer and activator of transcription 1 <br />', 'symbol': u'Stat1', 'species': u'Mus musculus', 'aliases': u'2010005J02Rik AA408197'}, {'ensembl_id': u'ENSMUSG00000040033', 'db_id': 46, 'description': u'signal transducer and activator of transcription 2 <br />', 'symbol': u'Stat2', 'species': u'Mus musculus', 'aliases': u'1600010G07Rik AW496480'}, {'ensembl_id': u'ENSMUSG00000004040', 'db_id': 46, 'description': u'signal transducer and activator of transcription 3 <br />', 'symbol': u'Stat3', 'species': u'Mus musculus', 'aliases': u'1110034C02Rik AW109958 Aprf'}, {'ensembl_id': u'ENSMUSG00000062939', 'db_id': 46, 'description': u'signal transducer and activator of transcription 4 <br />', 'symbol': u'Stat4', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000004043', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5A <br />', 'symbol': u'Stat5a', 'species': u'Mus musculus', 'aliases': u'AA959963 STAT5'}, {'ensembl_id': u'ENSMUSG00000020919', 'db_id': 46, 'description': u'signal transducer and activator of transcription 5B <br />', 'symbol': u'Stat5b', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000002147', 'db_id': 46, 'description': u'signal transducer and activator of transcription 6 <br />', 'symbol': u'Stat6', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000024271', 'db_id': 46, 'description': u'elongation protein 2 homolog (S. cerevisiae) <br />', 'symbol': u'Elp2', 'species': u'Mus musculus', 'aliases': u'AU023723 Epl2 StIP1 Statip1'}, {'ensembl_id': u'ENSMUSG00000026946', 'db_id': 46, 'description': u'N-myc (and STAT) interactor <br />', 'symbol': u'Nmi', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000032405', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 1 <br />', 'symbol': u'Pias1', 'species': u'Mus musculus', 'aliases': u'2900068C24Rik Ddxbp1 GBP'}, {'ensembl_id': u'ENSMUSG00000025423', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 2 <br />', 'symbol': u'Pias2', 'species': u'Mus musculus', 'aliases': u'6330408K17Rik AI462206 ARIP3 AU018068 Dib Miz1 PIASxalpha PIASxb PIASxbeta SIZ2'}, {'ensembl_id': u'ENSMUSG00000028101', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 3 <br />', 'symbol': u'Pias3', 'species': u'Mus musculus', 'aliases': u''}, {'ensembl_id': u'ENSMUSG00000004934', 'db_id': 46, 'description': u'protein inhibitor of activated STAT 4 <br />', 'symbol': u'Pias4', 'species': u'Mus musculus', 'aliases': u'PIASY Pias-gamma Piasg'}, {'ensembl_id': u'ENSMUSG00000056153', 'db_id': 46, 'description': u'suppressor of cytokine signaling 6 <br />', 'symbol': u'Socs6', 'species': u'Mus musculus', 'aliases': u'1500012M23Rik 5830401B18Rik AI447482 Cis4 Cish4 HSPC060 SOCS-4 SOCS-6 SSI4 STAI4 STATI4 Socs4'}], 'order': []} 
