"""

"""

from guide.model.stemformatics import *

from pylons import config

import subprocess




def test_get_dataset_mapping_id():

    result = Stemformatics_Dataset.get_dataset_mapping_id()
    assert len(result) >1 
    assert result[2000] == 21
    assert result[1000] == 7 


    ds_id = 0
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) >1 
    assert result[2000] == 21
    assert result[1000] == 7 

    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert "2000\t21\n" in text
    assert "1000\t7\n" in text

    ds_id = 2000
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) ==1 
    assert result[2000] == 21
    assert 1000 not in result
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert "2000\t21\n" in text
    assert "1000\t7\n" not in text

    ds_id = 23232322323
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) ==0 
    assert result == {}
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'No data found.'
    assert "1000\t7\n" not in text


    ds_id = 'result'
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) ==0 
    assert result == {}
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'No data found.'

    ds_id = []
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) ==0 
    assert result == {}
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'No data found.'

    ds_id = {} 
    result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
    assert len(result) ==0 
    assert result == {}
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'No data found.'


def test_text_for_download():
    result = 99
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'Invalid data.'

    result = 'nothing'
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'Invalid data.'

    result = []
    text = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)
    assert text == 'Invalid data.'


def test_refresh_probe_mappings():
    result = Stemformatics_Dataset.refresh_probe_mappings_to_download()
    assert 'COPY 22090' in result
    assert 'COPY 0' in result
    assert 'COPY 50490' in result
    cmd = "ls -alh /var/www/html/mappings"
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    output = p.stdout.read()
    p.stdout.close()
    assert 'mapping_66.txt'  in output
    assert 'mapping_7.txt'  in output
    assert 'mapping_21.txt'  in output
