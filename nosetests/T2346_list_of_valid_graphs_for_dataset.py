from guide.model.stemformatics import *
from guide.model.graphs import *
import psycopg2
import psycopg2.extras


def test_datasets():

    # all these datasets have been manually calculated using dev psql 

    ds_id = 5003
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['box','bar','scatter']


    ds_id = 6081
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['box','bar','scatter','violin']


    ds_id = 6083
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['box','bar','scatter','violin']


    ds_id = 6185
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['line','box','bar','scatter']

    ds_id = 6880
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['box','bar','scatter']

    ds_id = 7072
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['box','bar','scatter']

    ds_id = 5008
    result = Stemformatics_Dataset.list_of_valid_graphs_for_dataset(ds_id)
    assert result == ['line','box','bar','scatter']
