from guide.model.stemformatics import *
from pylons import config
import redis
import json
ds_id = 2000
uid = 3
import re

def test_4():

    

    string_of_usernames = ['nothing']
    result = Stemformatics_Auth.get_users_from_usernames(string_of_usernames)
    assert result == []

    string_of_usernames = 'nothing'
    result = Stemformatics_Auth.get_users_from_usernames(string_of_usernames)
    assert result == []


    string_of_usernames = 2312321
    result = Stemformatics_Auth.get_users_from_usernames(string_of_usernames)
    assert result == []

    list_of_usernames = ['rowland@stemformatics.org']
    result = Stemformatics_Auth.get_users_from_usernames(list_of_usernames)
    assert len(result) == 1

    for row in result:
        assert row['uid'] == 3
        assert row['status'] == 1

    string_of_usernames = 'rowland@stemformatics.org'
    result = Stemformatics_Auth.set_users_to_be_inactive_from_usernames(string_of_usernames)
    assert len(result) == 1

    string_of_usernames = 'nothing@newbi.org guest@stemformatics.org rowland@stemformatics.org'
    result = Stemformatics_Auth.set_users_to_be_inactive_from_usernames(string_of_usernames)
    assert len(result) == 2



    string_of_usernames = 'asdfasf@nnn.com nothing@newbi.org\n guest@stemformatics.org\rrowland@stemformatics.org'
    list_of_usernames  = re.findall("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+", string_of_usernames)
    assert len(list_of_usernames) == 4


    


