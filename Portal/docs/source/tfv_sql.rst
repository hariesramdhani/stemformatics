
==================================================
Transcription Framework Viewer SQL Database Schema
==================================================

 * Nick Seidenman
 * Matthew Anderson (original MySQL schema)

As part of the collaborative effort between Griffith University (Nathan) and WEHI, a database
schema has been evolving that can be used with the genome and transcription
data mining and rendering tools developed by these institutions.  Every effort was made to construct 
both an architecture is modular (and therefore flexible), 
and a body of data to store therein which could be used, with little or no
schematic modification by both groups.   With respect to genomic and assay platform (chip) data, 
this goal has been achieved.   The sample (experiment) datasets of interest to these groups does, however,
differ greatly and as a consequence it is much more difficult to design an architecture in which 
both can comfortably fit.   This write-up, then, 
will focus on the parts that are common: the genomic and chip data. 
The `TFV ORM Design <tfv_orm_des.html>`_ paper will describe
the organization of data specific to WEHI.

This PostgreSQL version of the schema is derived from Matthew Anderson's  
TFV (transcript framework viewer) 
database schema as provided 14 June 2010.  Matt's version was written for
a MySQL implementation, however current and future development will employ
PostgreSQL.

In addition to translation to PostgreSQL, this version also contains a number of
changes to Matt's original schema, including the addition of several tables
and sequences to support dynamic enumerated types.  The diagram below attempts
to depict the relationships between the tables and sequences in this implementation.

.. image:: magma-orgc.png

One major change is in the way indexes are defined.  MySQL allows them
to be defined within the table definition, almost as though they were
just another column or column constraint.  Indexes are defined outside
of (and AFTER) the tables that they're indexing.

It also contains additional functions and triggers that handle database 
column transformations (typically on insertions) and integrity checks.

Although this is more or less specific to PostgreSQL,
the syntax has been kept as close to SQL95 as possible, in anticipation of the need to
someday, perhaps, migrate this yet again to some other DBMS such as SQLite.
PostgreSQL 8.4.3 was used at the outset of this project, and as of this writing PostgreSQL 
9.0rc1 is the current version in  use.

A great deal of commentary text has been included within the DDL file(s) with 
particular attention given to explaining changes or additions to the original (MySQL)
version of the schema.

Ensembl Gene: The "Gold Standard"
=================================

At the current time it has been decided that Ensembl will be the authoritative
reference source for all genomic information.  Consequently, there are numerous
references to and assumptions based on the use of 
`Ensembl Gene <http://www.ensembl.org/>`_ (i.e. 
`BioMart <http://www.ensembl.org/biomart>`_-derived)
annotation data.

An ensemble instance has been set up on one of the development machines at WEHI, however,
the organization of this database, and its heavy reliance on MySQL "dialect" make it
unsuitable for direct use by our models at this time.  Consequently, for the foreseable future,
that portion of data originating with Ensembl will be extracted using biomart.

(Sub)schemas
============

The database itself can be called whatever seems appropriate to the systems
designers / architects.  During development it was called "mattay" in a nod to 
the original creator but has been otherwise named in subsequent production 
environments.  

There are several subschemas (none the less called "schemas") within the database
that serve to segregate more or less inter-related groups of tables.  In Matt's original
schema there was only one subschema -- TFV.   In this PostgreSQL version, others
have been added both to show that they are supplemental to the original, "core" schema
and to indicate closer inter-relation and similar functional purpose of the tables that constitute
these other schemas.

TFV Schema
----------

The original database defined only one schema -- called TFV -- and much of its
original structure is preserved herein.  This section details the schema and
explains changes or additions to it that appear in the PostgreSQL version.  The full
schema DDL can be found in `here <tfv2-pg.sql>`_.

Starting Anew
.............

It is assumed that we want to start with a clean slate, so any previous incarnation
of this schema is first removed.  Since an existing schema may have tables (i.e. dependent
children) we use the ``CASCADE`` modifier to drop any and all of those as well.  Creation
of the schema can then proceed.  Note that it is  created in the default tablespace, but there
is no reason some other tablespace could not be used.   Also note that ``SET SEARCH_PATH`` is the 
PostgreSQL equivalent of ``USE``

.. code-block:: sql

 DROP SCHEMA TFV CASCADE;
 CREATE SCHEMA  TFV;
 SET SEARCH_PATH TO TFV;


One other structure change that should be noted early on is the rearrangement of the order in which several
tables are defined.   The assay_platforms, genome and probe annotation and mapping tables have been moved to the forefront,
and those tables that store genelist information are defined toward the end.  For WEHI's purposes they aren't used
at all, but they are still included and are used by Griffith developers and may, someday be used by WEHI as well.

Supporting Enumerations and Sequence(s)
.......................................

These appeared as ENUM types in the MySQL version.  Although postgres
supports a similar construct, using a table like this one allows
new enumerands to be added without having to modify the schema itself.  
It also provides simple but effective  integrity checking since an attempt
to insert a value in a column that references such an *enumerating table* which is
not found in the enumeration will trigger a FOREIGN KEY constraint violation.

.. code-block:: sql

 DROP TABLE gender_enum;
 DROP TABLE species_enum;
 DROP TABLE feature_type;
 DROP SEQUENCE type_seq;
  
 
 CREATE TABLE TFV.gender_enum (
        label TEXT PRIMARY KEY
 );
 
 INSERT INTO TFV.gender_enum VALUES ('Unkown'); 
 INSERT INTO TFV.gender_enum VALUES ('Male'); 
 INSERT INTO TFV.gender_enum VALUES ('Female');  
 INSERT INTO TFV.gender_enum VALUES ('XX'); 
 INSERT INTO TFV.gender_enum VALUES ('XY'); 
 
 CREATE TABLE TFV.species_enum (
        label TEXT PRIMARY KEY
 );
 
 INSERT INTO TFV.species_enum VALUES ('Human');
 INSERT INTO TFV.species_enum VALUES ('Mouse');
 INSERT INTO TFV.species_enum VALUES ('Drosophilla');
 ``

General-use Anto-increenting Unique ID: AUTOINCREMENT vs SERIAL
...............................................................

This originally appeared as chip_type INT(11) AUTOINCREMENT
in Matt's schema.  PostgreSQL has a corresponding data type -- ``SERIAL`` --
but using an external sequence provides a bit better control 
and referencing options, so I prefer to use that instead.   Sequences are 
also transaction-safe (i.e. lastval() will always return the same value within
the scope of a transaction even if other, parallel transactions are using the sequence)
so it's good practice to use this semantic rather than the ``SERIAL`` type.

The ``INCREMENT BY (n)`` is an artifact of the GUI Matt used to create this
schema.  There's no particular need for it, but I've retained it to 
in the unlikely event that it should actually matter somehow.  (It also harks back
to the stone-age days of computer science when we used statement numbers and,
prudently, used intervals of 10 or 100 in the (utterly likely) event that we'd later
need to insert other statements and avoid the painful process of having to renumber
all subsequent statements.)

As this version evolved, the need arose for such unique IDs for other columns
in other tables.  Since there's no intrinsic meaning in the
sequence values, there's no reason not to use a single sequence
for these columns, so I changed chip_type_seq to type_seq and
changed the increment to 5.

.. code-block:: sql

 CREATE SEQUENCE type_seq
        INCREMENT BY 5;

assay_platforms
...............

Other than chip_type, nearly identical to MySQL schema.  Chip_type was formerly called ``type`` however this is a reserved word in PostgreSQL
and so its use, though not strictly forbidden, should be avoided.  Hence the name change.  It's worth
noting that this table served as a model for the annotation_databases table, below.

.. code-block:: sql

 DROP TABLE IF EXISTS TFV.assay_platforms ;
 
 CREATE TABLE TFV.assay_platforms (
   chip_type INT DEFAULT nextval('type_seq') PRIMARY KEY,
   species TEXT NOT NULL REFERENCES species_enum(label),
   manufacturer TEXT NULL DEFAULT NULL ,
   platform TEXT NULL DEFAULT NULL ,
   version TEXT NULL DEFAULT NULL 
 );


annotation_databases
....................

In the original TFV schema several other tables used genome_version, annotator, annotation_version, and model_id,
explicitly, as both a PRIMARY KEY set and as a FOREIGN KEY constraint.   using four fields collectively this way
can get tedious, particularly when querying these tables or when trying to populate them and still maintain referential
integrity.   To make things a good bit easier (and, I think, clearer), these four items are now grouped into their own table,
here, and each configuration is assigned its own, unique ID, which is now referenced as a single, scalar foreign key
in other tables.

.. code-block:: sql

 DROP TABLE IF EXISTS TFV.annotation_databases; 
 
 CREATE TABLE TFV.annotation_databases (
   an_database_id INT PRIMARY KEY DEFAULT nextval('type_seq'),
   genome_version TEXT NOT NULL,
   annotator TEXT NOT NULL,
   annotation_version TEXT NOT NULL,
   model_id TEXT NOT NULL
 );


genome_mappings
...............

This is where the vast majority of searches will probably begin.  It maps the various symbols and
IDs associated with a gene or transcript to the Ensembl Gene or Ensembl Transcript ID that will 
become the key to other annotation retrievals and other relations.   By convention (rule is such 
an ugly word, don't you think?) the `left` or `annotator` side of the table will contain the Ensembl
ID for the gene or transcript being described.   The `right` or `annotator2` side will contain
the corresponding symbols and IDs (e.g. MGI ID or HGNC ID) that is being mapped to the Ensembl ID. 
There will be one row for each ID or symbol being mapped to Ensembl.  This includes an "identity" mapping
that takes the Ensembl ID to itself, thereby simplying queries that may initiate from a user entering an Ensembl
ID straightaway.

.. table::

 +----------------------+---------------------+
 | annotator1 info      | annotator2 info     |
 |  (dbID, ens gene ID) |  (dbID, symbol)     |
 +----------------------+---------------------+

.. code-block:: sql

 DROP TABLE IF EXISTS TFV.genome_mappings ;
 
 CREATE TABLE TFV.genome_mappings (
   annotator_db1 INT REFERENCES TFV.annotation_databases(an_database_id),
   annotator_id1 TEXT NOT NULL DEFAULT '' ,
   annotator_db2 INT REFERENCES TFV.annotation_databases(an_database_id),
   annotator_id2 TEXT NOT NULL DEFAULT '' ,
   fspk SERIAL PRIMARY KEY
  );
 
 CREATE INDEX genome_mappings_id1_ndx ON TFV.genome_mappings(annotator_id1);
 CREATE INDEX genome_mappings_id1_ndx ON TFV.genome_mappings(annotator_id2);


Searching on annotator_id2 will return 0 or more columns with the
results of the annotator*1 columns pointing to the (for now) Ensembl
genome information.  Searching on ann*1 with Ensembl gene ID will
return associated symbols, synonyms and other accession data for
this gene in other databases.  Examples depicting how this works in practice
can be found in the :ref:`Examples` section, below.

Genes versus Transcripts
++++++++++++++++++++++++

The rows in the ``TFV.genome_annotations`` table are populated from an `Ensembl BioMart <http://ensembl.org/>` query
that includes gene `and` transcript information.  Consequently, a query for a particular transcript
will return not just the transcript annotation(s) but those of the gene of which each transcript is a part.
The immediate import is that symbol -> Ensembl transcript ID and an identity mapping are also stored in 
genome_mappings.

genome_annotations
..................

This table's definition
is derived from an `Ensembl BioMart <http://www.ensembl.org/biomart>`_ query that includes the 
following features:

 * Ensemble Gene ID
 * Transcript ID
 * Description
 * Chromosome Name
 * Gene Start (bp)
 * Gene End (bp)
 * Strand
 * Band
 * Transcript Start (bp)
 * Transcript End (bp)
 * Associated Gene Name
 * Associated Transcript Name
 * Associated Gene DB
 * Associated Transcript DB
 * Transcript count
 * % GC content
 * Gene Biotype
 * Source 
 * Status (gene)
 * Status (transcript)
 * Entrez ID
 * MGI ID
 * Ref Seq ID

Note that for now ONLY gene information -- as opposed to transcript information -- is included.  This was to keep 
the development environment simple, and also to give developers time to decide whether or not to use
a separate table for transcripts (probably) or include transcript data in this table as well.  The table definition itself is:

.. code-block:: sql 

 create table TFV.genome_annotations (
     db_id int references TFV.annotation_databases (an_database_id),
     gene_id text ,
     transcript_id text,
     Description text ,
     Chromosome_Name text ,
     Gene_start_bp int ,
     Gene_End_bp int ,
     Strand int ,
     Band text ,
     Transcript_start_bp int,
     Transcript_end_bp int,
     Associated_Gene_Name text ,
     Associated_Transcript_Name text ,
     Associated_Gene_DB text ,
     Associated_Transcript_DB text,
     Transcript_count int,
     percent_GC_content numeric ,
     Gene_Biotype text ,
     Source text ,
     Gene_status text ,
     EntrezGene_ID text ,
     MGI_ID text ,
     RefSeq_DNA_ID text,
     primary key (Gene_ID, Transcript_ID, EntrezGene_ID, MGI_ID, RefSeq_DNA_ID)
 );

 create index genes_chr_name_ndx on TFV.genome_annotations (chromosome_name);
 create index genes_start_bp_ndx on TFV.genome_annotations (Gene_Start_bp);
 create index genes_end_bp_ndx on TFV.genome_annotations (Gene_End_bp);
 create index genes_pct_gc_cont_ndx on TFV.genome_annotations (percent_gc_content);

genome_locations
................

This table used to hold only the extracted gene location (chromosome, start & end basepairs, orientation) for each
gene annotation found in the *``genes.*`* tables.  This is now contained in the ``genome_locations`` 
table, and the genome_annotations table now contains what used to be the ``TFV.genome_annotations`` table.
There is no longer be a ``genes`` schema at all.  
*ALL* genome (gene, transcript, exon) annotation data can and will
now be stored in ``genome_annotations``.


.. code-block:: sql

 DROP TABLE IF EXISTS TFV.feature_enum;
 
 CREATE TABLE TFV.feature_enum (
   name text primary key
 );
 
 INSERT INTO TFV.feature_enum VALUES ('gene');
 INSERT INTO TFV.feature_enum VALUES ('transcript');
 INSERT INTO TFV.feature_enum VALUES ('exon');


 DROP TABLE IF EXISTS TFV.genome_locations;
 
 CREATE TABLE TFV.genome_locations (
   database_id INT NOT NULL REFERENCES TFV.annotation_databases(an_database_id),
   identifier TEXT NOT NULL DEFAULT '' ,
   chromosome TEXT NULL DEFAULT NULL ,
   strand INT DEFAULT NULL ,
   start_bp INT  DEFAULT NULL ,
   end_bp INT  DEFAULT NULL ,
   feature text REFERENCES TFV.feature_enum(name)
   PRIMARY KEY (database_id, identifier) );

 CREATE INDEX TFV_locii_id_ndx ON TFV.genome_annotations (identifier);
 CREATE INDEX TFV_locii_chr_ndx ON TFV.genome_annotations (chromosome);
 CREATE INDEX TFV_locii_sbp_ndx ON TFV.genome_annotations (start_bp);
 CREATE INDEX TFV_locii_ebp_ndx ON TFV.genome_annotations (end_bp);


probe_annotations
.................

.. code-block:: sql

 DROP TABLE IF EXISTS TFV.probe_annotations; 
 
 CREATE TABLE TFV.probe_annotations (
   chip_type INT NOT NULL DEFAULT 0 ,
   probe_id TEXT NOT NULL DEFAULT '0' ,
   probe_sequence TEXT NULL DEFAULT NULL ,
   PRIMARY KEY (chip_type, probe_id) ,
   CONSTRAINT probe_annotations_ibfk_1
     FOREIGN KEY (chip_type )
     REFERENCES TFV.assay_platforms (chip_type )
     ON DELETE NO ACTION
     ON UPDATE NO ACTION);
 
 CREATE INDEX probe_annotations_chip_type ON TFV.probe_annotations(chip_type);


probe_mappings
..............

.. code-block:: sql

 DROP TABLE IF EXISTS TFV.probe_mappings ;
 
 CREATE TABLE TFV.probe_mappings (
   chip_type INT NOT NULL DEFAULT 0 ,
   probe_id TEXT NOT NULL DEFAULT '0' ,
   database_id INT REFERENCES TFV.annotation_databases(an_database_id),
   identifier TEXT NOT NULL ,
   mapped INT DEFAULT NULL, -- COMMENT percentage of probe sequence mapped to annotation. ,
   PRIMARY KEY (chip_type, probe_id, database_id, identifier),
   FOREIGN KEY (chip_type , probe_id ) REFERENCES TFV.probe_annotations (chip_type , probe_id )
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
   FOREIGN KEY (database_id, identifier) REFERENCES TFV.genome_annotations (database_id, identifier)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
 );
 
 CREATE INDEX probe_mappings_id_ndx ON TFV.probe_mappings(identifier);



Additional Subschemas
=====================

In its current form, this schema contains two, new subschemas:  *genes* and *probes*.  Both of these
were adopted from earlier versions of a schema that was being developed at WEHI prior to Matt Anderson's
TFV (version 2) schema becoming available.   They contain the more or less complete annotations available from
their authoritative sources.

Probes Schema
-------------

The *probes* subschema is intended to hold probe annotation data that can be linked to gene annotations (typically via
the tfv.probe_mappings table) and otherwise rendered in 
higher-level interfaces layered over top of the SQL data store(s).  (See 
`TFV Object Relational Mapping <tfv_orm_des.html>`_.)
Each of the tables in it would contain an annotation set obtained or derived from an authoritative (one would hope) source such as Illumina or Affymetrix.
The name of each annotation table will be the same as the tfv.assay_platforms.platform column.  This will allow quick, if indirect association of chip_id (or chip_type)
with the corresponding annotation table.

The anticipated interface to this database will be an Object-Relational Mapper (ORM) of some sort.   As this infers an object-oriented paradigm, it further assumes
that the annotations within this schema will be regarded as polymorphic versions of some abstract base class of "probe".  Typically, a given **gene** object will have 
attributed to it a collection of **probe** objects, however, these may not all be the same type of probe.  The collection that is readily (via the tfv.probe_mappings table)
available to constitute this collection includes objects with attributes *chip_id* and *probe_id*.  Chip_id can be used to identify the appropriate annotation table
(via reference to tfv.assay_platforms.platform column) and so provides a criterion upon which a polymorphism can be determined.  Thus, rather than having a collection of what are
essentially just rows from the tfv.probe_mappings table, the gene object can have a heterogeneous collection of probe annotation-like objects, each of which being populated with
data from the proper table.   It will then be left to the ORM layer(s) to homogenize the namespace and API that will be used to access disparately-named or missing columns.
This will be described in greater detail in 
`TFV Object Relational Mapping <tfv_orm_des.html>`_.

Illumina Annotations:  probes.illumina
......................................

The **probes.illumina** table is derived from data supplied by Illumina for their 
`MouseWG-6 Version 2 <http://www.illumina.com/gsp/downloads/MouseWG-6_V2_0_R1_11278593_A.zip>`_ product.  
To see if the table format was compatible with that of the MouseWG-6 product, 
the HumanWG-6 (V3) data was loaded into the same table without error.  It should be noted that
the MouseWG-6 data set includes one additional feature -- obsolete_probe_id -- not found in the HumanWG-6 data.  As this is the last column in the mouse data, its absence results in a
``NULL`` value in this column for human data.

Unlike its affymetrix counterpart, these data do not include Ensembl gene references, thus, retrieval of this information must go through the tfv.probe_mappings table.  '

Here is the table definition:

.. code-block:: sql

 create table probes.illumina (
     Species text ,
     Source text ,
     Search_Key text ,
     Transcript text ,
     ILMN_Gene text ,
     Source_Reference_ID text ,
     RefSeq_ID text ,
     Unigene_ID text ,
     Entrez_Gene_ID text ,
     GI text ,
     Accession text ,
     Symbol text ,
     Protein_Product text ,
     Probe_Id text primary key,
     Array_Address_Id text ,
     Probe_Type text ,
     Probe_Start text ,
     Probe_Sequence text ,
     Chromosome text ,
     Probe_Chr_Orientation text ,
     Probe_Coordinates text ,
     Cytoband text ,
     Definition text ,
     Ontology_Component text ,
     Ontology_Process text ,
     Ontology_Function text ,
     Synonyms text ,
     Obsolete_Probe_Id text
  );

Affymetrix Annotations:  probes.affymetrix
..........................................

The **probes.affymetrix** table is derived from data supplied by Affymetrix for their 
`Mouse 430-2 <http://www.affymetrix.com/analysis/downloads/na30/ivt/Mouse430_2.na30.annot.csv.zip>`_ product.  
Unlike its illumina counterpart, these data include Ensembl gene references, however, to keep the programming 
API simple, access to either table is indexed through
the tfv.probe_mappings table.  

The table definition is identical to that which would be used for human genome probe 
annotation data and to confirm this
the HG-U133A (V2) annotations were loaded into the same table definition.  
No errors were encountered when this was loaded into an empty table, however,
the probe annotation identified
by probe_id ``AFFX-BioB-3_at`` did trigger a primary key uniqueness constraint violation 
when it was loaded after the Mouse430-2 annotations.

Whereas the Illumina data comes in a TSV (tab-separated version) format that can, with the removal of the first (column heading) line be used, without any other modification, to populate the probes.illumina dataset, the Affymetrix data come in CSV (comma-separated version) format and include additional header / commentary information.  Moreover, each column entry
is enclosed in quotation marks.   Thus, some pre-processing must be done on these data before they can be used to populate the probes.affymetrix table.

There was one important change made to the column names:  *Probe Set Name* in the original annotation file is called *probe_id* in the table.  This is to allow polymorphic instantiation of higher-level probe objects based on probe_id (along with chip_id).  This wasn't necessary for Illumina data (probes.illumina) since the probe ID column was
already called *probe_id* to begin with.

Here is the probes.affymetrix table DDL:

.. code-block:: sql

 create table probes.affymetrix (
     -- Probe_ID is Probe_Set_Name in the raw input, but to make polymorphism work 
     -- This column must have the same name in all probes.* tables.
     Probe_ID text , 
     GeneChip_Array text ,
     Species_Scientific_Name text ,
     Annotation_Date text ,
     Sequence_Type text ,
     Sequence_Source text ,
     Transcript_ID_lp_Array_Design_rp_ text ,
     Target_Description text ,
     Representative_Public_ID text ,
     Archival_UniGene_Cluster text ,
     UniGene_ID text ,
     Genome_Version text ,
     Alignments text ,
     Gene_Title text ,
     Gene_Symbol text ,
     Chromosomal_Location text ,
     Unigene_Cluster_Type text ,
     Ensembl text ,
     Entrez_Gene text ,
     SwissProt text ,
     EC text ,
     OMIM text ,
     RefSeq_Protein_ID text ,
     RefSeq_Transcript_ID text ,
     FlyBase text ,
     AGI text ,
     WormBase text ,
     MGI_Name text ,
     RGD_Name text ,
     SGD_accession_number text,
     Gene_Ontology_Biological_Process text default '',
     Gene_Ontology_Cellular_Component text default '',
     Gene_Ontology_Molecular_Function text default '',
     Pathway text default '',
     InterPro text default '',
     Trans_Membrane text default '',
     QTL text default '',
     Annotation_Description text default '',
     Annotation_Transcript_Cluster text default '',
     Transcript_Assignments text default '',
     Annotation_Notes text default '',
     primary key (probe_id)
 );


Probe set tabular information for this data set can be found in the
`Technical Affymetrix Formats manual <http://www.affymetrix.com/support/technical/manual/taf_manual.affx>`_
on the Affymetrix website.

Gene Schema
-----------

Although Ensembl serves as the source of core authoritative annotations, the Entrez Gene annotations,
by `NCBI at the US National Institutes of Health <http://ncbi.nih.gov/>` provides additional annotation
and naming data that are widely used.  Consequently, it is well worth including this data in the 
genome annotations provided by TFV / Magma.   The Entrez Gene database has therefore been added to
our database, included in its own table which itself exists in a subschema separate from the 
`TFV`, `probes`, and other schemas.   The Entrez ID serves as the *primary key* column for this table making
it easy to join to ``genome_annotations`` using that table's EntrezGene_ID column.   Indeed, the ORM
defines SQL JOINs between these two tables, along with ``genome_mappings`` to construct the
raw data source that constitutes :ref:`EnsemblGene` and :ref:`EnsemblTranscript` objects.

Note that in an earlier version of this database there was a `genes` schema that contained Ensembl
annotation information.  That schema has since been deprecated and its constituent ``ensembl`` table is
is now ``TFV.genome_annotations``.  This new `gene` schema should not, therefore, be confused with the
other `genes` schema of yore.



Building and Loading
====================

The current database is built by invoking a shell script called ``build.sh`` and providing the name of the 
database (default is `magma`) to build.  A dozen or so additional files containing DDL and raw data are
also used in building the core database (annotations and mappings) and SHA1 checksums of these files are
compared against a manifest to verify that they have not been altered prior to starting the build.  The script
performs checks at various stages and, in the event of an error, will exit with an error message and status
rather than proceeding to the next stage of the build process.

Expression Data
===============

This should probably be its own document, but for now here's a *proposed* concept of 
how expression data can be stored and retrieved. from this same database.   
Much of it is built on experience gained from the development of the TFV, probes, and genes subschemas,
described above, particularly the method for dynamically determining which detail 
dataset (i.e. table) to reference for a specific sample.

What is becoming clear is that more time needs to be spent thinking about the `meta-data` aspects of
storing and retrieving expression data and desiging a schema based on these ideas.  For now, this
proposed schema will allow elementary expression searches to be conducted to at least demonstrate
the sort of annotation / signal fusion that is possible with this database.

The *datasets* Subschema
----------------------------

Datasets as such do not need to reside in the same database or even on the same server as the rest of the TFV database.   In fact, there are good reasons why they should probably be kept in separate databases.  For example, access to some datasets may be available only to a restricted group of users. Keeping these datasets on a different server, in a different database provides two, additional layers of protection over and above that provided by user-based RDBMS access controls.   In the event that the expression data will reside in the same database, it is still important to keep it segregated and to do this, we introduce the *datasets* subschema.   

Whether or not it resides in a separate database, the schema defines a minimum of three tables:  ``experiments``, ``summary`` and ``summary_lmh``. 

The ``experiments`` Table
.........................

``experiments`` is analogous to the ``tfv.assay_platforms`` and ``tfv.annotation_databases`` tables in that it assigns a unique (integer) ID to each dataset that will be held in this subschema.   The table is defined as:

.. code-block:: sql

 create table datasets.experiments (
     ds_id int primary key,
     lab text not null default 'unknown',
     chip_type int references tfv.assay_platforms(chip_type),
     dtg timestamp not null default now(),
     handle text unique not null -- this should match the name of the table holding raw dataset.
 );


Like ``platform`` in ``tfv.assay_platforms``, ``handle`` here will, by convention, be required to be the same as the name of the table in *datasets* holding this particular dataset.  We may want to add ``tissue_type`` (and maybe even a separeate ``tissue_types`` enumeration table) to support a wider variety of tissue types, besides blood.  As each new dataset is added to the database, a new, corresponding row is added to this table for it.

Once an ``datasets.experiments`` record has been added, the dataset itself can be loaded.  The name of this dataset will be, as mentioned will be whatever ``datasets.experiments.handle`` has been set to.  The datasets we have seen so far follow the same general format as that used for the probe and genome annotations -- TSV with the first row being column headings -- so the same kind of processing used for those other files can be used for datasets to process them into these tables.   These files tend to all be organized with identifying information (typically probe ID), followed by columns that give signal data for that probe for one or more cell types.   Note that the specific types of cells and number of replicates thereof within each dataset can and will vary.   Sometimes the dataset will include a gene symbol column along with the probe ID, however access to genomic annotations should (and probably will) always be acquired through probe annoations.  

The dataset itself will not  be stored within this database.  The data will be extracted from it and digested into a ``summary`` table and a ``cell_types`` table.  The ``summary`` table will hold all the same data that were found in the dataset itself, but the columns are reorganized to enable more efficient loading and searching, as well as adding a "mean signal" colum.  This is explained further in the next section.

The ``summary`` Table
.....................

Since the datasets themselves are not of uniform (enough) format, searching them presents two problems.  First, one doesn't know which cell types are represented in these datasets, or what their order of appearance will be in the dataset's columns.  Second, the number of replicates for each cell type represented in a dataset can vary, making searches all the more highly-specific to a given dataset.  Finally, in all cases discussed so far (not to mention the current version of *GuIDE*) it is the arithmetic mean of the replicate signals, rather than any one signal, that is used when querying for high or low expression levels or ranges.  Thus, to use a raw dataset for queries means either that we code specific queries for each dataset, or maintain additional *meta-information* for each dataset that can be used for a more generalized library of query functions (i.e. stored procedures) and that, these efficiencies aside, we'll still need to compute averages across replicates as a part of each and every query operation.  This is simply too inefficient.

An alternative approach would be to create a table that is composed of a fixed number of columns that will, together, identify a specific, mean signal for a given probe and cell type.  Such a table would look like this:

.. code-block:: sql

 create table datasets.summary (
        ds_id int,
        cell_type text,
        gene_symbol text,
        probe_id text,
        signal float,
        replicates float[]
        primary key (ds_id,cell_type,gene_symbol,probe_id)
 );

The advantage can be seen in that we can now perform a query such as:

.. code-block:: sql

 select * from datasets.summary where cell_type in {'lsk', 'neut'} and signal > 14.0;

In other words, we can search across whatever cell types for a given expression threshold.  
What's more, this same format works for all datasets, and so we load summary 
information for all datasets into this same table, allowing expression searches 
''across datasets *regardless of the number and type of cells found in any given dataset**.  
The ``cell_type``, ``probe_id``, and ``signal`` columns would all be indexed (B-trees) to 
facilitate very rapid high or low expression level threshold searches.    The breaking up 
of columns and the averaging of replicate expression levels could probably be done within 
the database itself if no gene symbol information is present.  However, the datasets we 
have seen to date all have this and all tend to put multiple symbols in the raw input.  
For example, here is one typical row from the ''Goodell'' (public) dataset:

::

 1415781_a_at    LOC100043459 /// LOC100044181 /// LOC100045245 /// LOC100046167 /// LOC672381 /// LOC675440 /// Sumo2   14.47   14.84   14.51   14.61   14.66   14.51   14.49   14.58   14.47   14.43   14.45   14.55   14.39   14.5    13.46   13.73   13.85   13.82   14.61   14.64


Notice that in the gene_symbol (second) column, we have several gene IDs 
delimited by a triple-slash (///).   We could process this with a 
`pl/pgsql <http://www.postgresql.org/docs/8.3/static/plpgsql.html>`_ procedure 
(an ``INSERT`` trigger would do nicely) but a tool was already written in python to do this that also creates
the tables specific to the dataset being added, along with the ``INSERT`` statements for the ``experiments`` 
table and other bookkeeping, including looking looking at the pattern of names in the first column to 
determine cell types and number of replicates for each, and then computing mean values for these, 
later, when it builds the ``summary`` table entries, later.

''Author's Note:  I'm still debating as to whether and how I could write such a function that would be installed and a trigger set up with just the loading of the raw dataset.''

The ``summary_lmh`` Table
...........................

Often, we are doing differential searches, looking for expression signals that are higher than some other highest, lowest, or mean levels found for other probes or cell types.   To accelerate these searches, we also construct a low-middle-high expression signals table very similar to the ``summary`` table, called ``summary_lmh``:

.. code-block:: sql

 create table datasets.summary_lmh (
        ds_id int references datasets.experiments(ds_id),
        cell_type text,
        gene_symbol text,
        low float,
        mean float,
        high float,
        primary key (ds_id,cell_type,gene_symbol)
 );

This last table is derived directly from the ``summary`` table and is populated with the following query:

.. code-block:: sql

 inert into datasets.summary_lmh
     select ds_id,cell_type,gene_symbol,min(signal),avg(signal),max(signal)
         from datasets.summary
         group by ds_id,cell_type,gene_symbol;

Since these will be extrema and means across all summarized datasets, this table must necessarily be reconstructed each time a new dataset is loaded.  Since happens relatively rarely, and generally takes less than 15  minutes per dataset loaded, this is not a big concern and can be done as part of the dataset batch-loading process.

.. _Examples:

Examples
========

Example 1: Gene Symbol Search
-----------------------------

The following query will retrieve entries from the ''tfv.genome_mappings'' table
that have annotator_id2 values that begin with 'Myb'.

.. code-block:: sql

 SELECT * from tfv.genome_mappings WHERE annotator_id2 LIKE 'Myb%';


yields:

::

  annotator_db1 |   annotator_id1    | annotator_db2 | annotator_id2 
 ---------------+--------------------+---------------+---------------
             21 | ENSMUSG00000002100 |            21 | Mybpc3        
             21 | ENSMUSG00000017861 |            21 | Mybl2         
             21 | ENSMUSG00000019982 |            21 | Myb           
             21 | ENSMUSG00000020061 |            21 | Mybpc1        
             21 | ENSMUSG00000025912 |            21 | Mybl1         
             21 | ENSMUSG00000038670 |            21 | Mybpc2        
             21 | ENSMUSG00000040463 |            21 | Mybbp1a       
             21 | ENSMUSG00000042451 |            21 | Mybph         
             21 | ENSMUSG00000068745 |            21 | Mybphl        
 (9 rows)


This query could (and will!) be joined with others to return gene and probe annotations.  For example, if 
we only want the ensembl gene id, and gene symbol concatenated with the gene description and transcript
count, we can use this query:

.. code-block:: sql

 SELECT DISTINCT annotator_id1,annotator_id2,description,transcript_count 
     FROM tfv.genome_mappings 
     JOIN TFV.genome_annotations ON (annotator_id1=gene_id AND annotator_id2 != gene_id)
     WHERE annotator_id2 LIKE 'Myb%';

producing:

::

    annotator_id1    | annotator_id2 |                                   description                                   | transcript_count 
 --------------------+---------------+---------------------------------------------------------------------------------+------------------
  ENSMUSG00000020061 | Mybpc1        | myosin binding protein C, slow-type Gene [Source:MGI (curated);Acc:MGI:1336213] |                7
  ENSMUSG00000019982 | Myb           | myeloblastosis oncogene Gene [Source:MGI Symbol;Acc:MGI:97249]                  |                2
  ENSMUSG00000025912 | Mybl1         | myeloblastosis oncogene-like 1 Gene [Source:MGI Symbol;Acc:MGI:99925]           |                2
  ENSMUSG00000017861 | Mybl2         | myeloblastosis oncogene-like 2 Gene [Source:MGI (curated);Acc:MGI:101785]       |                6
  ENSMUSG00000038670 | Mybpc2        | myosin binding protein C, fast-type Gene [Source:MGI Symbol;Acc:MGI:1336170]    |                1
  ENSMUSG00000068745 | Mybphl        | myosin binding protein H-like Gene [Source:MGI (curated);Acc:MGI:1916003]       |                1
  ENSMUSG00000042451 | Mybph         | myosin binding protein H Gene [Source:MGI Symbol;Acc:MGI:1858196]               |                1
  ENSMUSG00000002100 | Mybpc3        | myosin binding protein C, cardiac Gene [Source:MGI (curated);Acc:MGI:102844]    |                3
  ENSMUSG00000040463 | Mybbp1a       | MYB binding protein (P160) 1a Gene [Source:MGI (curated);Acc:MGI:106181]        |                8


Example 2: Return probe annotations associated with a given gene annotation
---------------------------------------------------------------------------

Suppose we wish to see what probes are associated with the gene identified by the symbol 'Myb':

.. code-block:: sql

 SELECT DISTINCT annotator_id1,annotator_id2,chip_type,probe_id FROM tfv.genome_mappings 
     JOIN tfv.probe_mappings ON (annotator_id1=identifier) 
     WHERE annotator_id2 = 'Myb' ORDER BY annotator_id1;


This returns:

::

    annotator_id1    | annotator_id2 | chip_type |   probe_id   
 --------------------+---------------+-----------+--------------
  ENSMUSG00000019982 | Myb           |         1 | ILMN_1231769
  ENSMUSG00000019982 | Myb           |         1 | ILMN_2683910
  ENSMUSG00000019982 | Myb           |         1 | ILMN_2752817
  ENSMUSG00000019982 | Myb           |        11 | 1421317_x_at
  ENSMUSG00000019982 | Myb           |        11 | 1422734_a_at
  ENSMUSG00000019982 | Myb           |        11 | 1450194_a_at


Notice that the probe IDs returned are for both Illumina and Affymetrix platforms.  We can use the
``chip_type`` to decide which probes.* table to query to obtain the actual probe annotations.  Which brings us
to ...

Example 3: Add probe annotation details to query results.
---------------------------------------------------------

Because probe annotations do not, for now, exist in a common format, some attention must be
paid as to which *probes.** table will be joined.  By convention, the ``platform`` column of the
tfv.assay_platforms must be the same as the name of the table that will contain that platform's annotations.
Since table names are case-insensitive, ``platform`` values can have any combination of upper- or lower-case
letters, so long as these values evaluate to valid table names.  

This function will use ``chip_id`` (first argument) to determine which table to fetch
the probe annotation from, using ``probe_id`` (second argument) as the key.


.. code-block:: sql

 create or replace function get_probe_annotation(ch_id int, pr_id text) returns  record  as $gpa$
     declare
         plat tfv.assay_platforms.platform%type;
         tbl_name text;
         crsr refcursor;
         probe_annot record;
     begin
         -- Get the table name from the assay_platforms table.
         select platform into plat from tfv.assay_platforms where chip_type=ch_id limit 1;
         tbl_name := 'probes.'||plat;
         -- A bit of PG magic:  create a cursor so that we can decide which table to
         -- select from on-the-fly.
         open crsr for execute 'select * from '||tbl_name||' where probe_id=*||pr_id||*';
         fetch crsr into probe_annot;
         close crsr;
         return probe_annot;
     end;
 $gpa$ language plpgsql;


The PostgreSQL ``RECORD`` type is a contrivance that provides a target for query results when
the exact column list and format are not known ahead of time.  Thus, ``probe_annot`` can hold
a *probes.illumina* row or a *probes.affymetrix* row or any other table row (or ``NULL`` for that matter).
Once defined, we can use this function in a ``SELECT``:

.. code-block:: sql

SELECT get_probe_annotation(1, 'ILMN_2683910'); 

which will return:

:: code-block::

                                          get_probe_annotation 
 --------------------------------------------------------------------------------------------------------------
 ("Mus musculus",RefSeq,ILMN_217498,ILMN_217498,MYB,NM_010848.3,NM_010848.3,"",17863,110556653,NM_010848.3,Myb,
     :
   [evidence IPI]","MGC18531; M16449; AI550390; c-myb","") 
 

(The result has been truncated as it is otherwise too long to depict accurately, here.)

Note two things:  First, the output looks as though there is only one column, and second, the row returned looks 
somewhat different that those returned in the first two examples.  This is a consequence of using the ``RECORD``
data type.  What we gain in flexibility we lose, somewhat, in naming and dereferencing.  None the less, this result can 
included, though not exactly joined, with those earlier examples:

.. code-block:: sql

 SELECT DISTINCT annotator_id1,annotator_id2,get_probe_annotation(chip_type,probe_id) FROM tfv.genome_mappings 
     JOIN tfv.probe_mappings ON (annotator_id1=identifier) 
     WHERE annotator_id2 = 'Myb' ORDER BY annotator_id1;

which will return:

::

 ENSMUSG00000019982 | Myb           | (1421317_x_at,"Mouse Genome 430 2.0 Array","Mus musculus",...)
 ENSMUSG00000019982 | Myb           | (1422734_a_at,"Mouse Genome 430 2.0 Array","Mus musculus",...)
 ENSMUSG00000019982 | Myb           | (1450194_a_at,"Mouse Genome 430 2.0 Array","Mus musculus",...)
 ENSMUSG00000019982 | Myb           | ("Mus musculus",MEEBO,ILMN_224219,ILMN_224219,MYB,...)
 ENSMUSG00000019982 | Myb           | ("Mus musculus",RefSeq,ILMN_217498,ILMN_217498,MYB,...)


(The results have been truncated to fit the page.  Actual results would wrap each row over several lines.)

***NOTE:*** Although this example demonstrates the ability to do this type of complex, hierarchical join using just
DDL and SQL statements and functions, in practice, this sort of querying will be managed at the ORM level.

Example 4: Expression Level Querying
------------------------------------

Suppose we want to come up with information on a list of genes that are highly (above some arbitrary threshold) expressed across all cell types in our datasets.  (Yes, this is contrived, and maybe not altogether realistic but it's close enough for horseshoes, so gimme a break.)

.. code-block:: sql

 select * from datasets.summary where signal > 16.82;

returns ...

::

  ds_id | probe_id | gene_symbol | cell_type | avg_signal |  replicates   
 -------+----------+-------------+-----------+------------+---------------
    100 |    64089 | Slc4a1      | N-Er      |      16.83 | {16.84,16.82}
 (1 row)

We can use the ``probe_id`` column to retrieve probe and (indirectly) genome annotations for this sample.  Here's a SQL ``join`` that will do just that:

.. code-block:: sql

 select cell_type,gene_symbol,gene_id,description from datasets.summary 
     join tfv.probe_mappings on (datasets.summary.probe_id=tfv.probe_mappings.probe_id) 
     join TFV.genome_annotations on (tfv.probe_mappings.identifier=TFV.genome_annotations.gene_id) 
     where signal > 16.82;

returns ... 

::

  cell_type | gene_symbol |      gene_id       |                                          description                                           
 -----------+-------------+--------------------+------------------------------------------------------------------------------------------------
  N-Er      | Slc4a1      | ENSMUSG00000006574 | solute carrier family 4 (anion exchanger), member 1 Gene [Source:MGI (curated);Acc:MGI:109393]
 (1 row)

We have the option to list any of the probe-specific information, but this will result in a rather long result set.

::

  cell_type | gene_symbol |      gene_id       |      description (truncated)      |  get_probe_annotation() (truncated)                                          
 -----------+-------------+--------------------+-----------------------------------+----------------------------------------------
  N-Er      | Slc4a1      | ENSMUSG00000006574 | solute carrier family 4 (anio ... | (1434502_x_at,"Mouse Genome 430 ...)
 (1 row)

As before, the results have been truncated to fit on the page.



