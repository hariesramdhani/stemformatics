__doc__ = """\

Setting up class for stemformatics

"""

import logging
log = logging.getLogger(__name__)


from sqlalchemy import *
from sqlalchemy.ext.sqlsoup import SqlSoup

from guide.model import meta

from pylons import config
from pylons import url
from pylons import request, response, session as beakerSess
from pylons import tmpl_context as c

from stemformatics_dataset import *
from stemformatics_expression import *
from stemformatics_gene import *
from stemformatics_transcript import *
from stemformatics_audit import *
from stemformatics_auth import *
from stemformatics_admin import *
from stemformatics_probe import *
from stemformatics_gene_set import *
from stemformatics_job import *
from stemformatics_shared_resource import *
from stemformatics_notification import * 
from stemformatics_ensembl_upgrade import * 
from stemformatics_help import * 
from stemformatics_export import * 
from stemformatics_msc_signature import * 

import zlib
import json


__all__ = [
    # From this module ...
    'db', 'engine', 'init_model',

    # From imported modules ...
    'Stemformatics_Dataset',
    'Stemformatics_Audit',
    'Stemformatics_Gene',
    'Stemformatics_Transcript',
    'Stemformatics_Expression',
    'Stemformatics_Probe',
    'Stemformatics_Admin',
    'Stemformatics_Auth',
    'Stemformatics_Export',
    'Stemformatics_Job',
    'Stemformatics_Shared_Resource',
    'Stemformatics_Ensembl_Upgrade',
    'Stemformatics_Notification',
    'Stemformatics_Msc_Signature',
    'Stemformatics_Gene_Set',
    'Stemformatics_Help'
    ]

db = None
engine = None


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def init_model(db_engine):

    global db, engine

    engine = db_engine
    
    log.debug('just setting up sqlsoup')
    
    db = SqlSoup(engine, use_labels=True)
    
    log.debug('just finished setting up sqlsoup')
    

