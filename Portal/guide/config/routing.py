"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'],
                 always_scan=config['debug'], explicit=True)
    map.minimization = False

    # The ErrorController route (handles 404/500 error pages); it should
    # likely stay at the top, ensuring it can always be resolved
    map.connect('/error/{action}', controller='error')
    map.connect('/error/{action}/{id}', controller='error')

    # CUSTOM ROUTES HERE
    map.connect('/project_grandiose', controller='projects', action='project_grandiose')
    map.connect('/leukomics', controller='projects', action='leukomics')


    map.connect('/', controller='contents', action='index')

    map.connect('/hamlet/index', controller='contents', action='removal_of_hamlet')
    map.connect('/tests', controller='main', action='tests')
    map.connect('/genes', controller='genes', action='search')
    map.connect('/genes/', controller='genes', action='search')
    map.connect('/genes/summary', controller='expressions', action='yugene_graph')

    map.connect('/workbench/gene_set_index', controller='genes', action='gene_set_index')
    map.connect('/workbench/public_gene_set_index', controller='genes', action='public_gene_set_index')
    map.connect('/workbench/gene_set_view/{id}', controller='genes', action='gene_set_view')
    map.connect('/workbench/gene_set_bulk_import_manager', controller='genes', action='gene_set_bulk_import_manager')
    map.connect('/workbench/merge_gene_sets', controller='genes', action='merge_gene_sets')

    map.connect('/admin/check_redis_consistency_for_datasets', controller='api', action='check_redis_consistency_for_datasets')

    map.connect('/workbench/histogram_wizard', controller='expressions', action='histogram_wizard')

    map.connect('/expressions', controller='contents', action='index')
    map.connect('/expressions/', controller='contents', action='index')

    map.connect('/datasets', controller='datasets', action='search')
    map.connect('/datasets/', controller='datasets', action='search')

    map.connect('/{controller}/{action}')

    map.connect('/{controller}/{action}/{id}')

    # Ensure a controller without an action maps to an index
    map.connect('/{controller}',controller='DYNAMIC',action='index')

    return map
