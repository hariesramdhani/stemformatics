#!/bin/bash

help () {

  echo "add_max_values_for_dataset.sh dev localhost portal_beta \"-p 5433\""

}


if [ "$#" -eq 0 ] 
    then
        help
        exit
fi

INSTANCE="$1"
DBSERVER="$2"
DB="$3"
PORT="$4"
cd /var/www/pylons-data/$INSTANCE/GCTFiles
for f in *.gct
do
    DS_ID=`echo "$f" | grep -o "[0-9]\+"`

    echo "Max value for dataset $DS_ID using file $f"
    MAXGRAPHVALUE=`tail -n+4 "$f" | cut -f3- | tr '\t' '\n' | grep -v "[a-zA-Z]" | sort -n | tail -n1`
    echo $MAXGRAPHVALUE
    SQL="INSERT INTO dataset_metadata values($DS_ID,'maxGraphValue','$MAXGRAPHVALUE');"
    echo $SQL
    psql -U portaladmin -h "$DBSERVER" "$DB" "$PORT" -c "$SQL"
    
done
