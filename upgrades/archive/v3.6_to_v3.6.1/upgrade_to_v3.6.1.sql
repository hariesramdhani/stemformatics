-- Task #820 - datasets that can have blank values, can create HC but nothing else
delete from dataset_metadata where ds_name = 'genePatternAnalysisAccess';
insert into dataset_metadata values (6070,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6071,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6073,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6075,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6076,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6081,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6082,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6083,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6084,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6089,'genePatternAnalysisAccess','Disable');
insert into dataset_metadata values (6111,'genePatternAnalysisAccess','Disable'); 




-- This is for task 826 line graph
delete from dataset_metadata where ds_id = 5037 and ds_name = 'lineGraphOrdering';
delete from biosamples_metadata where ds_id = 5037 and md_name in ('Day','LineGraphGroup');
insert into biosamples_metadata values(72,'6033111110_A','Day','Day 0',5037);
insert into biosamples_metadata values(72,'6033111110_B','Day','Day 02',5037);
insert into biosamples_metadata values(72,'6033111110_C','Day','Day 05',5037);
insert into biosamples_metadata values(72,'6033111110_D','Day','Day 08',5037);
insert into biosamples_metadata values(72,'6033111110_E','Day','Day 11',5037);
insert into biosamples_metadata values(72,'6033111110_F','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111110_G','Day','Day 18',5037);
insert into biosamples_metadata values(72,'6033111110_H','Day','ES',5037);
insert into biosamples_metadata values(72,'6033111113_A','Day','iPSC1o',5037);
insert into biosamples_metadata values(72,'6033111113_B','Day','iPSC2o',5037);
insert into biosamples_metadata values(72,'6033111113_C','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_D','Day','Day 21',5037);
insert into biosamples_metadata values(72,'6033111113_E','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_F','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_G','Day','Day 21',5037);
insert into biosamples_metadata values(72,'6033111113_H','Day','Day 21',5037);
insert into biosamples_metadata values(72,'6033111110_A','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_B','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_C','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_D','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_E','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_F','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_G','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_H','LineGraphGroup','ES',5037);
insert into biosamples_metadata values(72,'6033111113_A','LineGraphGroup','iPSC1o',5037);
insert into biosamples_metadata values(72,'6033111113_B','LineGraphGroup','iPSC2o',5037);
insert into biosamples_metadata values(72,'6033111113_C','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_D','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_E','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_F','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_G','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_H','LineGraphGroup','Low',5037);
insert into dataset_metadata values(5037,'lineGraphOrdering','{"Day 0":1,"Day 02":2,"Day 05":3,"Day 08":4,"Day 11":5,"Day 16":6,"Day 18":7,"Day 21":8,"ES":9,"iPSC1o":10,"iPSC2o":11}');

-- -------------------------------------------------------------------------------------------------------------
delete from dataset_metadata where ds_id = 6073 and ds_name = 'lineGraphOrdering';
delete from biosamples_metadata where ds_id = 6073 and md_name in ('Day','LineGraphGroup');
insert into biosamples_metadata values(101,'D0','Day','Day 0',6073);
insert into biosamples_metadata values(101,'D11','Day','Day 11',6073);
insert into biosamples_metadata values(101,'D16APG2r2','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16BPG2r2','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16PG2r1','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D18','Day','Day 18',6073); 
insert into biosamples_metadata values(101,'D21PG2r1','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxMinus','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxPlus','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D2','Day','Day 02',6073);
insert into biosamples_metadata values(101,'D5','Day','Day 05',6073);
insert into biosamples_metadata values(101,'D8','Day','Day 08',6073);
insert into biosamples_metadata values(101,'IPS1o','Day','iPSC1o',6073);
insert into biosamples_metadata values(101,'IPS2o','Day','iPSC2o',6073);
insert into biosamples_metadata values(101,'rtTA','Day','ES',6073);
insert into biosamples_metadata values(101,'D0','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D11','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D16APG2r2','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16BPG2r2','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16PG2r1','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D18','LineGraphGroup','High',6073); 
insert into biosamples_metadata values(101,'D21PG2r1','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxMinus','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxPlus','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D2','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D5','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D8','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'IPS1o','LineGraphGroup','iPSC1o',6073);
insert into biosamples_metadata values(101,'IPS2o','LineGraphGroup','iPSC2o',6073);
insert into biosamples_metadata values(101,'rtTA','LineGraphGroup','ES',6073);
insert into dataset_metadata values(6073,'lineGraphOrdering','{"Day 0":1,"Day 02":2,"Day 05":3,"Day 08":4,"Day 11":5,"Day 16":6,"Day 18":7,"Day 21":8,"ES":9,"iPSC1o":10,"iPSC2o":11}');

