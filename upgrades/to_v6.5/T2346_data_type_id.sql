-- this is to add data_type_id to datasets for highlighting single cell datasets for violin plot
-- it is based off T#2364 and data_types table in agile_org

alter table datasets add column data_type_id int default 0 not null;

update datasets set data_type_id = 9 where id = 6081; 
update datasets set data_type_id = 9 where id = 6083;                              
update datasets set data_type_id = 3 where id = 6599;                              
update datasets set data_type_id = 3 where id = 6673;                              
update datasets set data_type_id = 3 where id = 6735;                              
update datasets set data_type_id = 3 where id = 6843;                              
update datasets set data_type_id = 3 where id = 6890;                              
update datasets set data_type_id = 3 where id = 6891;                              
update datasets set data_type_id = 3 where id = 6892;                              
update datasets set data_type_id = 3 where id = 6893;                              
update datasets set data_type_id = 3 where id = 6899;                              
update datasets set data_type_id = 3 where id = 6900;                              
update datasets set data_type_id = 3 where id = 6909;                              
update datasets set data_type_id = 3 where id = 6911;                              
update datasets set data_type_id = 3 where id = 6913;                              
update datasets set data_type_id = 3 where id = 6936;                              
update datasets set data_type_id = 3 where id = 6939;                              
update datasets set data_type_id = 3 where id = 6945;                              
update datasets set data_type_id = 3 where id = 6946;                              
update datasets set data_type_id = 3 where id = 6955;                              
update datasets set data_type_id = 3 where id = 6956;                              
update datasets set data_type_id = 3 where id = 6957;                              
update datasets set data_type_id = 3 where id = 6958;                              
update datasets set data_type_id = 3 where id = 6959;                              
update datasets set data_type_id = 3 where id = 6967;                              
update datasets set data_type_id = 3 where id = 6969;                              
update datasets set data_type_id = 3 where id = 6970;                              
update datasets set data_type_id = 3 where id = 6977;                              
update datasets set data_type_id = 3 where id = 6980;                              
update datasets set data_type_id = 3 where id = 6983;                              
update datasets set data_type_id = 3 where id = 6994;                              
update datasets set data_type_id = 3 where id = 6996;                              
update datasets set data_type_id = 3 where id = 6999;                              
update datasets set data_type_id = 3 where id = 7033;                              
update datasets set data_type_id = 3 where id = 7054;                              
update datasets set data_type_id = 3 where id = 7032;                              
update datasets set data_type_id = 3 where id = 7034;                              
update datasets set data_type_id = 3 where id = 7035;                              
update datasets set data_type_id = 3 where id = 7038;                              
update datasets set data_type_id = 3 where id = 7042;                              
update datasets set data_type_id = 3 where id = 7043;                              
update datasets set data_type_id = 3 where id = 7044;                              
update datasets set data_type_id = 3 where id = 7055;                              
update datasets set data_type_id = 3 where id = 7056;                              
update datasets set data_type_id = 3 where id = 7057;                              
update datasets set data_type_id = 3 where id = 7058;                              
update datasets set data_type_id = 3 where id = 7071;                              
update datasets set data_type_id = 3 where id = 7074;                              
update datasets set data_type_id = 3 where id = 7075;                              
update datasets set data_type_id = 3 where id = 7085;                              
update datasets set data_type_id = 3 where id = 7098;                              
update datasets set data_type_id = 3 where id = 7104;                              
update datasets set data_type_id = 3 where id = 7105;                              
update datasets set data_type_id = 3 where id = 7106; 

select id,handle from datasets where data_type_id in (3,9);
