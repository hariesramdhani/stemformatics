#!/bin/sh
## OK 2011-07-21
## Dataset stats update first-run / incremental script.
## Run without args for usage info.
##
dsid="$1"
pgpassfile="$2"
firstrun="$3"

datetime=`date "+%Y%m%d-%H%M%S"`
if [ ! -w `dirname $0` ]; then
  logfile="/tmp/update_stats_for_datasets_$datetime.log"
else
  logfile=`dirname $0`
  logfile="$logfile/update_stats_for_datasets_$datetime.log"
fi


Usage () {
  echo "Usage: $0 <dataset_id[,..]> <pg-passfile> [-f|--firstrun]"
  echo
  echo "    NOTES:"
  echo "    * If 'firstrun' flag given, ALL gene <-> sample detection stats in target DB will be RESET."
  echo "    * Target dataset/s must already be loaded in DB instance referenced by 'pg-passfile'."
  echo "    * Target pg-passfile MUST be chmod 600 and owned by user running this script."
  echo "    * If any input dataset/s have already had stats generated, they will be skipped."
  echo
  exit 1
}

load_db_settings () {
  pgpass="$1"
  if [ -f "$pgpass" ]; then
    export PGHOST=`awk -F':' '{print $1}' $pgpass`
    export PGPORT=`awk -F':' '{print $2}' $pgpass`
    export PGNAME=`awk -F':' '{print $3}' $pgpass`
    export PGUSER=`awk -F':' '{print $4}' $pgpass`
    export PGPASSFILE="$pgpass"
  else
    echo "Error: Target 'pg-passfile' not found!"; echo
    exit 1
  fi
}

exec_firstrun_dataset () {
  dataset="$1"
  logfile="$2"
  if [ ! -z "$dataset" ]; then
    echo "Performing 'firstrun' stats refresh with dataset: $dataset"
    time cat stats_update_firstrun.sql | sed -r -e "s/%DATASET_ID%/$dataset/g" | psql -U $PGUSER -h $PGHOST -p $PGPORT $PGNAME >> $logfile 2>&1
  fi
  echo "Stats update complete for dataset $dataset."
}

exec_incremental_dataset () {
  dataset="$1"
  logfile="$2"
  if [ ! -z "$dataset" ]; then
    echo; echo "Processing dataset: $dataset"
    ## Check if dataset stats were already performed
    ret=`echo "select * from stemformatics.dataset_chip_type_map where ds_id=$dataset" | psql -U $PGUSER -h $PGHOST -p $PGPORT $PGNAME`
    ### DEBUG ###
    #echo "ret=[$ret]"
    ### END ###
    echo "$ret" | grep "0 rows" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      time cat stats_update_increment.sql | sed -r -e "s/%DATASET_ID%/$dataset/g" | psql -U $PGUSER -h $PGHOST -p $PGPORT $PGNAME >> $logfile 2>&1

    ## If dataset stats already exist for this dataset, skip
    else
      echo "  WARN: Dataset $dataset stats already in database! Skipping."; echo
    fi
  fi
  echo "Stats update complete for dataset $dataset."
  echo "===="
}

### MAIN ###

## By default, we'll perform an incremental dataset stats update
do_firstrun="false"

if [ -z "$dsid" ]; then
  Usage
fi

## Set multiple datasets if given (comma-separated list)
if [ `echo "$dsid" | grep ","` ]; then
  export dsids=`echo "$dsid" | sed -r -e "s/\,/ /g"`
  echo "Performing stats update for datasets: $dsids"
  ## DEBUG ##
  #echo "Input has multiple datasets!"; echo
  #for d in $dsids; do
  #  echo "Got dataset [$d]"
  #done
  ## END ##
else
  export dsids=""
fi

## Stats are being refreshed for the first time, still requires a target dataset to begin.
if [ "$firstrun" = "-f" -o "$firstrun" = "--firstrun" ]; then
  echo -n "Are you SURE you want to run the 'firstrun' build? This will NUKE all statistics and start from scratch? [y/N] ";
  read ans
  case $ans in
    Y|y)
      do_firstrun="true"
      ;; 
    *) 
      echo "Aborting stats update."; echo; exit
      ;;
  esac
fi


load_db_settings "$pgpassfile"

echo "(Log file: $logfile)"; echo


if [ "$do_firstrun" = "true" ]; then
  ## Multiple comma-separated datasets given, do "firstrun" on first,
  ## then remaining ones incrementally.
  if [ ! -z "$dsids" ]; then
    count=0
    for d in $dsids; do
      if [ $count -eq 0 ]; then
        exec_firstrun_dataset "$d" "$logfile"
      else
        exec_incremental_dataset "$d" "$logfile"
      fi
      count=`expr $count + 1`
    done
  else
    exec_firstrun_dataset "$dsid" "$logfile"
  fi

## Incremental dataset stats update
else
  if [ -z "$dsids" ]; then
    export dsids="$dsid"
  fi
  for d in $dsids; do
    exec_incremental_dataset "$d" "$logfile"
  done
fi

echo "All Done."; echo
