CREATE TABLE stemformatics.gene_sets
(
  id bigserial NOT NULL,
  gene_set_name character(250) NOT NULL,
  db_id integer NOT NULL,
  uid integer NOT NULL,
  CONSTRAINT pk_gene_sets PRIMARY KEY (id),
  CONSTRAINT unique_gene_sets UNIQUE (gene_set_name)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE stemformatics.gene_set_items
(
  id bigserial NOT NULL,
  gene_set_id bigint NOT NULL,
  gene_id character(100) NOT NULL,
  CONSTRAINT pk_gene_set_items PRIMARY KEY (id),
  CONSTRAINT unique_gene_items UNIQUE (gene_set_id,gene_id)
)
WITH (
  OIDS=FALSE
);
