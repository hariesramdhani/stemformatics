ALTER TABLE stemformatics.jobs ADD COLUMN finished timestamp without time zone default NULL;

-- update the current jobs to have the finished set to be 10 minutes after created if their status is completed (1)
update stemformatics.jobs set finished = created + interval '10 minutes' where status = 1;
